﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiondestock.BL
{
    class CLS_Produits
    {
        private dbStockContext db = new dbStockContext();
        private Produit P; // table Client 
        public bool Ajouter_Produit(string NomProduit, int quantite, string prix, byte[] ImageProduit, int idcategorie)
        {
            P = new Produit(); // nouveau Client
            P.Nom_Produit = NomProduit;
            P.Quantite_Produit = quantite;
            P.Prix_Produit = prix;
            P.ID_GATEGORIE= idcategorie;
            P.Image_Produit = ImageProduit;

            //verifier si le nom et le prenom existe déj) dans la base de donnée 
            if (db.Produit.SingleOrDefault(s => s.Nom_Produit == NomProduit) == null) // si n'existe pas
            {
                db.Produit.Add(P);// ajouter dans la table Produit
                db.SaveChanges();// save dans BD
                return true;
            }
            else // si existe dans le BD
            {
                return false;
            }
        }
        public void modifier_Produit(int idp, string NomProduit, int quantite, string prix, byte[] ImageProduit, int idcategorie)
        {
            P = new Produit();
            P = db.Produit.SingleOrDefault(s => s.Id_Produit == idp); // verifier si id de client est existe 
            if (P != null)//si existe 
            {
                P.Nom_Produit = NomProduit;
                P.Quantite_Produit = quantite;
                P.Prix_Produit = prix;
                P.ID_GATEGORIE = idcategorie;
                P.Image_Produit = ImageProduit;
                db.SaveChanges(); // save nv info dans bd


            }
        }
        // fonction suprimer Produit 
        public void suprimer_Client(int id)
        {
            P = new Produit();
            P = db.Produit.SingleOrDefault(P => P.Id_Produit == id);
            db.Produit.Remove(P);
            db.SaveChanges();


        }


    }
}
