﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiondestock.BL
{
    class D_Commande
    {
        // sauvgarder les produits commander dans listes 
        public static List<D_Commande> listeDetial = new List<D_Commande>();
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Quantite { get; set; }
        public string Prix { get; set; }
        public String Remise { get; set; }
        public string Totale { get; set; }

    }
}
