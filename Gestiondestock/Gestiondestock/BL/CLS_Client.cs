﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiondestock.BL
{
    class CLS_Client
    {
        private dbStockContext db = new dbStockContext();
        private Client C; // table Client 
        public bool Ajouter_Client(string Nom , string prenom , string Adresse , string Telephone , string Email , string pays , string ville )
        {
            C = new Client(); // nouveau Client
            C.Nom_client = Nom;
            C.Prenom_Client = prenom;
            C.Adresse_Client = Adresse;
            C.telephone_Client = Telephone;
            C.Email_Client = Email;
            C.Pays_Client = pays;
            C.Ville_CLient = ville;
            //verifier si le nom et le prenom existe déj) dans la base de donnée 
            if(db.Client.SingleOrDefault(s=>s.Nom_client==Nom && s.Prenom_Client==prenom)==null) // si n'existe pas
            {
                db.Client.Add(C);// ajouter dans la table client
                db.SaveChanges();// save dans BD
                return true;
            }
            else // si existe dans le BD
            {
                return false;
            }
        }
        //fonction modifier dans le base de donnée 
        public void modifier_client (int id , string Nom, string prenom, string Adresse, string Telephone, string Email, string pays, string ville)
        {
            C = new Client();
            C = db.Client.SingleOrDefault(s => s.ID_Client == id); // verifier si id de client est existe 
            if(C!=null)//si existe 
            {
                C.Nom_client = Nom; // nv nom 
                C.Prenom_Client = prenom; // nv prenom 
                C.Adresse_Client = Adresse;
                C.telephone_Client = Telephone;
                C.Email_Client = Email;
                C.Pays_Client = pays;
                C.Ville_CLient = ville;
                db.SaveChanges(); // save nv info dans bd


            }
        }
        // fonction suprimer Client 
        public void suprimer_Client( int id)
        {
            C = new Client();
            C = db.Client.SingleOrDefault(s => s.ID_Client == id);
            db.Client.Remove(C);
            db.SaveChanges();


        }
    
    }
}
