﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestiondestock.BL
{
    class CLS_Categorie
    {
        private dbStockContext db = new dbStockContext();
        private Categorie Cat; // table categoire 
        public bool Ajouter_Categorie(string NomCategorie)
        {
            Cat = new Categorie(); // nouveau catégorie
            Cat.Nom_Categorie = NomCategorie;
           
            //verifier si le nom et le prenom existe déj) dans la base de donnée 
            if (db.Categorie.SingleOrDefault(s => s.Nom_Categorie == NomCategorie) == null) // si n'existe pas
            {
                db.Categorie.Add(Cat);// ajouter dans la table catégorie
                db.SaveChanges();// save dans BD
                return true;
            }
            else // si existe dans le BD
            {
                return false;
            }
        }
        public void modifier_Categorie(int idCategorie, string NomCategorie)
        {
            Cat = new Categorie();
            Cat = db.Categorie.SingleOrDefault(s => s.ID_GATEGORIE == idCategorie); // verifier si id de client est existe 
            if (Cat != null)//si existe 
            {
                Cat.Nom_Categorie = NomCategorie; // nv catég
                db.SaveChanges(); // save nv info dans bd


            }
        }
        public void suprimer_Categorie(int id)
        {
            Cat = new Categorie();
            Cat = db.Categorie.SingleOrDefault(s => s.ID_GATEGORIE == id);
            db.Categorie.Remove(Cat);
            db.SaveChanges();


        }
    }
}
