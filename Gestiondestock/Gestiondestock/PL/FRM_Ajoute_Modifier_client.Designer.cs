﻿
namespace Gestiondestock.PL
{
    partial class FRM_Ajoute_Modifier_client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LBtitre = new System.Windows.Forms.Label();
            this.textNomClient = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textPrenomClient = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textTELCLIENT = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textEmailClient = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.textVilleClient = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.textPaysClient = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnannuller = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textAdresseClient = new System.Windows.Forms.TextBox();
            this.btnActualiser = new System.Windows.Forms.Button();
            this.btnEnregistre = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 3);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 625);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(710, 3);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 622);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(707, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(3, 622);
            this.panel4.TabIndex = 3;
            // 
            // LBtitre
            // 
            this.LBtitre.AutoSize = true;
            this.LBtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBtitre.Location = new System.Drawing.Point(18, 23);
            this.LBtitre.Name = "LBtitre";
            this.LBtitre.Size = new System.Drawing.Size(263, 44);
            this.LBtitre.TabIndex = 4;
            this.LBtitre.Text = "Ajouter Client";
            this.LBtitre.Click += new System.EventHandler(this.LBtitre_Click);
            // 
            // textNomClient
            // 
            this.textNomClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textNomClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNomClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textNomClient.Location = new System.Drawing.Point(71, 106);
            this.textNomClient.Multiline = true;
            this.textNomClient.Name = "textNomClient";
            this.textNomClient.Size = new System.Drawing.Size(272, 38);
            this.textNomClient.TabIndex = 6;
            this.textNomClient.Text = "Nom de Client";
            this.textNomClient.Enter += new System.EventHandler(this.textNomClient_Enter);
            this.textNomClient.Leave += new System.EventHandler(this.textNomClient_Leave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel5.Location = new System.Drawing.Point(76, 142);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(250, 1);
            this.panel5.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel6.Location = new System.Drawing.Point(415, 141);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(250, 1);
            this.panel6.TabIndex = 11;
            // 
            // textPrenomClient
            // 
            this.textPrenomClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textPrenomClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPrenomClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrenomClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textPrenomClient.Location = new System.Drawing.Point(410, 105);
            this.textPrenomClient.Multiline = true;
            this.textPrenomClient.Name = "textPrenomClient";
            this.textPrenomClient.Size = new System.Drawing.Size(272, 38);
            this.textPrenomClient.TabIndex = 9;
            this.textPrenomClient.Text = "Prenom de Client";
            this.textPrenomClient.TextChanged += new System.EventHandler(this.textPrenomClient_TextChanged);
            this.textPrenomClient.Enter += new System.EventHandler(this.textPrenomClient_Enter);
            this.textPrenomClient.Leave += new System.EventHandler(this.textPrenomClient_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel7.Location = new System.Drawing.Point(415, 242);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(250, 1);
            this.panel7.TabIndex = 14;
            // 
            // textTELCLIENT
            // 
            this.textTELCLIENT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textTELCLIENT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textTELCLIENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTELCLIENT.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textTELCLIENT.Location = new System.Drawing.Point(410, 206);
            this.textTELCLIENT.Multiline = true;
            this.textTELCLIENT.Name = "textTELCLIENT";
            this.textTELCLIENT.Size = new System.Drawing.Size(272, 38);
            this.textTELCLIENT.TabIndex = 12;
            this.textTELCLIENT.Text = "Telephone de Client";
            this.textTELCLIENT.TextChanged += new System.EventHandler(this.textTELCLIENT_TextChanged);
            this.textTELCLIENT.Enter += new System.EventHandler(this.textTELCLIENT_Enter);
            this.textTELCLIENT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTELCLIENT_KeyPress);
            this.textTELCLIENT.Leave += new System.EventHandler(this.textTELCLIENT_Leave);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel8.Location = new System.Drawing.Point(415, 333);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(250, 1);
            this.panel8.TabIndex = 17;
            // 
            // textEmailClient
            // 
            this.textEmailClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textEmailClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textEmailClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEmailClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textEmailClient.Location = new System.Drawing.Point(410, 297);
            this.textEmailClient.Multiline = true;
            this.textEmailClient.Name = "textEmailClient";
            this.textEmailClient.Size = new System.Drawing.Size(272, 38);
            this.textEmailClient.TabIndex = 15;
            this.textEmailClient.Text = "Email de Client";
            this.textEmailClient.Enter += new System.EventHandler(this.textEmailClient_Enter);
            this.textEmailClient.Leave += new System.EventHandler(this.textEmailClient_Leave);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel9.Location = new System.Drawing.Point(415, 434);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(250, 1);
            this.panel9.TabIndex = 20;
            // 
            // textVilleClient
            // 
            this.textVilleClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textVilleClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textVilleClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textVilleClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textVilleClient.Location = new System.Drawing.Point(410, 398);
            this.textVilleClient.Multiline = true;
            this.textVilleClient.Name = "textVilleClient";
            this.textVilleClient.Size = new System.Drawing.Size(272, 38);
            this.textVilleClient.TabIndex = 18;
            this.textVilleClient.Text = "Ville  Client";
            this.textVilleClient.Enter += new System.EventHandler(this.textVilleClient_Enter);
            this.textVilleClient.Leave += new System.EventHandler(this.textVilleClient_Leave);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel11.Location = new System.Drawing.Point(74, 433);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(250, 1);
            this.panel11.TabIndex = 26;
            // 
            // textPaysClient
            // 
            this.textPaysClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textPaysClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPaysClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPaysClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textPaysClient.Location = new System.Drawing.Point(69, 397);
            this.textPaysClient.Multiline = true;
            this.textPaysClient.Name = "textPaysClient";
            this.textPaysClient.Size = new System.Drawing.Size(272, 38);
            this.textPaysClient.TabIndex = 24;
            this.textPaysClient.Text = "Pays Client";
            this.textPaysClient.Enter += new System.EventHandler(this.textPaysClient_Enter);
            this.textPaysClient.Leave += new System.EventHandler(this.textPaysClient_Leave);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Gestiondestock.Properties.Resources.Country_32;
            this.pictureBox8.Location = new System.Drawing.Point(24, 397);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(39, 38);
            this.pictureBox8.TabIndex = 25;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Gestiondestock.Properties.Resources.adresse;
            this.pictureBox7.Location = new System.Drawing.Point(26, 196);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(39, 38);
            this.pictureBox7.TabIndex = 22;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Gestiondestock.Properties.Resources.ville_32;
            this.pictureBox6.Location = new System.Drawing.Point(365, 398);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(39, 38);
            this.pictureBox6.TabIndex = 19;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Gestiondestock.Properties.Resources.Gmail_32;
            this.pictureBox5.Location = new System.Drawing.Point(365, 297);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(39, 38);
            this.pictureBox5.TabIndex = 16;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Gestiondestock.Properties.Resources.Phone_32;
            this.pictureBox4.Location = new System.Drawing.Point(365, 206);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 38);
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Gestiondestock.Properties.Resources.login_32;
            this.pictureBox3.Location = new System.Drawing.Point(365, 105);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 38);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Gestiondestock.Properties.Resources.login_32;
            this.pictureBox2.Location = new System.Drawing.Point(26, 106);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 38);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::Gestiondestock.Properties.Resources.Button_Delete_icon;
            this.btnannuller.Location = new System.Drawing.Point(662, 9);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(39, 38);
            this.btnannuller.TabIndex = 5;
            this.btnannuller.TabStop = false;
            this.btnannuller.Click += new System.EventHandler(this.btnannuller_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel10.Location = new System.Drawing.Point(74, 333);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(250, 1);
            this.panel10.TabIndex = 28;
            // 
            // textAdresseClient
            // 
            this.textAdresseClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textAdresseClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textAdresseClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdresseClient.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textAdresseClient.Location = new System.Drawing.Point(69, 196);
            this.textAdresseClient.Multiline = true;
            this.textAdresseClient.Name = "textAdresseClient";
            this.textAdresseClient.Size = new System.Drawing.Size(272, 131);
            this.textAdresseClient.TabIndex = 27;
            this.textAdresseClient.Text = "Adresse Client";
            this.textAdresseClient.Enter += new System.EventHandler(this.textAdresseClient_Enter);
            this.textAdresseClient.Leave += new System.EventHandler(this.textAdresseClient_Leave);
            // 
            // btnActualiser
            // 
            this.btnActualiser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnActualiser.FlatAppearance.BorderSize = 0;
            this.btnActualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualiser.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnActualiser.Location = new System.Drawing.Point(69, 503);
            this.btnActualiser.Name = "btnActualiser";
            this.btnActualiser.Size = new System.Drawing.Size(257, 55);
            this.btnActualiser.TabIndex = 29;
            this.btnActualiser.Text = "Actualiser";
            this.btnActualiser.UseVisualStyleBackColor = false;
            this.btnActualiser.Click += new System.EventHandler(this.btnActualiser_Click);
            // 
            // btnEnregistre
            // 
            this.btnEnregistre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnEnregistre.FlatAppearance.BorderSize = 0;
            this.btnEnregistre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistre.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEnregistre.Location = new System.Drawing.Point(410, 503);
            this.btnEnregistre.Name = "btnEnregistre";
            this.btnEnregistre.Size = new System.Drawing.Size(255, 55);
            this.btnEnregistre.TabIndex = 30;
            this.btnEnregistre.Text = "Enregistre ";
            this.btnEnregistre.UseVisualStyleBackColor = false;
            this.btnEnregistre.Click += new System.EventHandler(this.btnEnregistre_Click);
            // 
            // FRM_Ajoute_Modifier_client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(710, 628);
            this.Controls.Add(this.btnEnregistre);
            this.Controls.Add(this.btnActualiser);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.textAdresseClient);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.textPaysClient);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.textVilleClient);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.textEmailClient);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.textTELCLIENT);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textPrenomClient);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.textNomClient);
            this.Controls.Add(this.btnannuller);
            this.Controls.Add(this.LBtitre);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRM_Ajoute_Modifier_client";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_Ajoute_Modifier_client";
            this.Load += new System.EventHandler(this.FRM_Ajoute_Modifier_client_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox btnannuller;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnEnregistre;
        public System.Windows.Forms.Label LBtitre;
        public System.Windows.Forms.Button btnActualiser;
        public System.Windows.Forms.TextBox textNomClient;
        public System.Windows.Forms.TextBox textPrenomClient;
        public System.Windows.Forms.TextBox textTELCLIENT;
        public System.Windows.Forms.TextBox textVilleClient;
        public System.Windows.Forms.TextBox textPaysClient;
        public System.Windows.Forms.TextBox textAdresseClient;
        public System.Windows.Forms.TextBox textEmailClient;
    }
}