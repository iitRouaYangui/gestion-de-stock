﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace Gestiondestock.PL
{
    public partial class FRM_Ajoute_Modifier_client : Form
    {
        private UserControl usrClient;
        public FRM_Ajoute_Modifier_client( UserControl usrC)
        {
            InitializeComponent();
            this.usrClient = usrC;
        }
        // les champs obligatoire 
        string   TestObligatoire()
        {
            if (textNomClient.Text == "" || textNomClient.Text == "Nom de Client")
            {
                return "Entrer Le Nom de Client";
            }
            if (textPrenomClient.Text == "" || textPrenomClient.Text == "Prenom de Client")
            {
                return "Entrer Le Prenom de Client ";
            }
            if (textAdresseClient.Text == "" || textAdresseClient.Text == "Adresse Client")
            {
                return "Entrer L'adresse Client ";
            }
            if (textTELCLIENT.Text == "Telephone de Client" || textTELCLIENT.Text == "Telephone de Client")
            {
                return "Entrer Le Telephone de Client ";
            }
            if (textEmailClient.Text == "" || textEmailClient.Text == "Email de Client")
            {
                return "Entrer L'email de Client ";
            }
            if (textPaysClient.Text == "" || textPaysClient.Text == "Pays Client")
            {
                return "Entrer Le Pays Client ";
            }
            if (textVilleClient.Text == "" || textVilleClient.Text == "Ville  Client")
            {
                return "Entrer La Ville  Client";
            }
            //verifier email valide ou nom 
            if  (textEmailClient.Text != "" || textEmailClient.Text != "Email de Client")
            {
                try
                {
                    new MailAddress(textEmailClient.Text); // pour verifier email si valid  ou nom
                }
                catch(Exception e)
                {
                    return " Email Invalide";
                }
            }
            return null;
        }
        private void FRM_Ajoute_Modifier_client_Load(object sender, EventArgs e)
        {

        }

        private void textNomClient_Enter(object sender, EventArgs e)
        {
            if(textNomClient.Text== "Nom de Client")
            {
                textNomClient.Text = "";
                textNomClient.ForeColor = Color.White;
            }
        }

        private void btnannuller_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textNomClient_Leave(object sender, EventArgs e)
        {
            if (textNomClient.Text == "")
            {
                textNomClient.Text = "Nom de Client";
                textNomClient.ForeColor = Color.Silver;
            }
        }

        private void textPrenomClient_Enter(object sender, EventArgs e)
        {
            if (textPrenomClient.Text == "Prenom de Client")
            {
                textPrenomClient.Text = "";
                textPrenomClient.ForeColor = Color.White;
            }
        }

        private void textPrenomClient_Leave(object sender, EventArgs e)
        {
            if (textPrenomClient.Text == "")
            {
                textPrenomClient.Text = "Prenom de Client";
                textPrenomClient.ForeColor = Color.Silver;
            }
        }

        private void textAdresseClient_Enter(object sender, EventArgs e)
        {
            if (textAdresseClient.Text == "Adresse Client")
            {
                textAdresseClient.Text = "";
                textAdresseClient.ForeColor = Color.White;
            }
        }

        private void textAdresseClient_Leave(object sender, EventArgs e)
        {
            if (textAdresseClient.Text == "")
            {
                textAdresseClient.Text = "Adresse Client";
                textAdresseClient.ForeColor = Color.Silver;
            }
        }

        private void textTELCLIENT_Enter(object sender, EventArgs e)
        {
            if (textTELCLIENT.Text == "Telephone de Client")
            {
                textTELCLIENT.Text = "";
                textTELCLIENT.ForeColor = Color.White;
            }
        }

        private void textTELCLIENT_Leave(object sender, EventArgs e)
        {
            if (textTELCLIENT.Text == "")
            {
                textTELCLIENT.Text = "Telephone de Client";
                textTELCLIENT.ForeColor = Color.Silver;
            }
        }

        private void textEmailClient_Enter(object sender, EventArgs e)
        {
            if (textEmailClient.Text == "Email de Client")
            {
                textEmailClient.Text = "";
                textEmailClient.ForeColor = Color.White;
            }
        }

        private void textEmailClient_Leave(object sender, EventArgs e)
        {
            if (textEmailClient.Text == "")
            {
                textEmailClient.Text = "Email de Client";
                textEmailClient.ForeColor = Color.Silver;
            }
        }

        private void textPaysClient_Enter(object sender, EventArgs e)
        {
            if (textPaysClient.Text == "Pays Client")
            {
                textPaysClient.Text = ""; 
                textPaysClient.ForeColor = Color.White;
            }
        }

        private void textVilleClient_Enter(object sender, EventArgs e)
        {
            if (textVilleClient.Text == "Ville  Client")
            {
                textVilleClient.Text = "";
                textVilleClient.ForeColor = Color.White;
            }
        }

        private void textPaysClient_Leave(object sender, EventArgs e)
        {
            if (textPaysClient.Text == "")
            {
                textPaysClient.Text = "Pays Client";
                textPaysClient.ForeColor = Color.Silver;
            }
        }

        private void textVilleClient_Leave(object sender, EventArgs e)
        {
            if (textVilleClient.Text =="" )
            {
                textVilleClient.Text = "Ville  Client";
                textVilleClient.ForeColor = Color.Silver;
            }
        }

        private void textTELCLIENT_KeyPress(object sender, KeyPressEventArgs e)
        {
            //textbox numeric
            if(e.KeyChar<48 || e.KeyChar>75) // asci des numéro
            {
                e.Handled = true;
            }
            if(e.KeyChar==8)
            {
                e.Handled = false;
            }
        }
        public int IdSelect ;
        // ajouter les nv client 
        private void btnEnregistre_Click(object sender, EventArgs e)
        {
            if(TestObligatoire() != null)
            {
                MessageBox.Show(TestObligatoire(),"Obligatoire",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {   if(LBtitre.Text== "Ajouter Client")
                {
                    BL.CLS_Client ClClient = new BL.CLS_Client(); // créer un objet pour stock dans BD
                    if (ClClient.Ajouter_Client(textNomClient.Text, textPrenomClient.Text, textAdresseClient.Text, textTELCLIENT.Text, textEmailClient.Text, textPaysClient.Text, textVilleClient.Text))
                    {
                        MessageBox.Show("Client Ajouter avec succes", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        (usrClient as User_Liste_Client).Actualiserdatagrid();
                    }
                    else
                    {
                        MessageBox.Show("Nom et Prenom de client déjà existant", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else // lb=modifier 
                {
                    BL.CLS_Client ClClient = new BL.CLS_Client();
                    User_Liste_Client user = new User_Liste_Client();
                    DialogResult R = MessageBox.Show("Voulez-vous vraiment modifier ce client", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if( R== DialogResult.Yes)
                    {
                        ClClient.modifier_client(IdSelect, textNomClient.Text, textPrenomClient.Text, textAdresseClient.Text, textTELCLIENT.Text, textEmailClient.Text, textPaysClient.Text, textVilleClient.Text);
                        // pour actualiser  datagrid view
                        (usrClient as User_Liste_Client).Actualiserdatagrid();
                        MessageBox.Show("Client Modifier avec succés","Modification", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Modification est anullé ", "Modification ",MessageBoxButtons.OK , MessageBoxIcon.Warning) ;
                    }
                    
                }
            }
                

        }

        private void btnActualiser_Click(object sender, EventArgs e)
        {
            textNomClient.Text = "Nom de Client";
            textNomClient.ForeColor = Color.Silver;

            textPrenomClient.Text = "Prenom de Client";
            textPrenomClient.ForeColor = Color.Silver;

            textAdresseClient.Text = "Adresse Client";
            textAdresseClient.ForeColor = Color.Silver;

            textTELCLIENT.Text = "Telephone de Client";
            textTELCLIENT.ForeColor = Color.Silver;

            textEmailClient.Text = "Email de Client";
            textEmailClient.ForeColor = Color.Silver;

            textPaysClient.Text = "Pays Client";
            textPaysClient.ForeColor = Color.Silver;

            textVilleClient.Text = "Ville  Client";
            textVilleClient.ForeColor = Color.Silver;
        }

        private void LBtitre_Click(object sender, EventArgs e)
        {

        }

        private void textPrenomClient_TextChanged(object sender, EventArgs e)
        {

        }

        private void textTELCLIENT_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
