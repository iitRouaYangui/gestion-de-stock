﻿
namespace Gestiondestock.PL
{
    partial class UserProduitControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dvgProduit = new System.Windows.Forms.DataGridView();
            this.Colimn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxRecherche = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSauvgarderdansUnexecl = new System.Windows.Forms.Button();
            this.btnImprimerToutes = new System.Windows.Forms.Button();
            this.btnImprimerPRcocher = new System.Windows.Forms.Button();
            this.btnafficherphotoProduit = new System.Windows.Forms.Button();
            this.btnsupprimerProduit = new System.Windows.Forms.Button();
            this.btnModifierProduit = new System.Windows.Forms.Button();
            this.btnAjouterProduit = new System.Windows.Forms.Button();
            this.btnActualiser = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dvgProduit)).BeginInit();
            this.SuspendLayout();
            // 
            // dvgProduit
            // 
            this.dvgProduit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvgProduit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvgProduit.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dvgProduit.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgProduit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dvgProduit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgProduit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Colimn1,
            this.id,
            this.Nom,
            this.Column1,
            this.Column2,
            this.Column3});
            this.dvgProduit.EnableHeadersVisualStyles = false;
            this.dvgProduit.Location = new System.Drawing.Point(6, 153);
            this.dvgProduit.Name = "dvgProduit";
            this.dvgProduit.RowHeadersVisible = false;
            this.dvgProduit.RowHeadersWidth = 51;
            this.dvgProduit.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            this.dvgProduit.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dvgProduit.RowTemplate.Height = 24;
            this.dvgProduit.Size = new System.Drawing.Size(1703, 430);
            this.dvgProduit.TabIndex = 17;
            this.dvgProduit.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgClient_CellContentClick);
            // 
            // Colimn1
            // 
            this.Colimn1.HeaderText = "Select";
            this.Colimn1.MinimumWidth = 6;
            this.Colimn1.Name = "Colimn1";
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Nom";
            this.Nom.MinimumWidth = 6;
            this.Nom.Name = "Nom";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Quantité";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Prix";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Catégorie ";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.panel3.Location = new System.Drawing.Point(343, 144);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(974, 3);
            this.panel3.TabIndex = 15;
            // 
            // textBoxRecherche
            // 
            this.textBoxRecherche.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxRecherche.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRecherche.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRecherche.ForeColor = System.Drawing.Color.DimGray;
            this.textBoxRecherche.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxRecherche.Location = new System.Drawing.Point(343, 102);
            this.textBoxRecherche.Multiline = true;
            this.textBoxRecherche.Name = "textBoxRecherche";
            this.textBoxRecherche.Size = new System.Drawing.Size(974, 39);
            this.textBoxRecherche.TabIndex = 14;
            this.textBoxRecherche.Text = "Recherche";
            this.textBoxRecherche.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxRecherche.TextChanged += new System.EventHandler(this.textBoxRecherche_TextChanged);
            this.textBoxRecherche.Enter += new System.EventHandler(this.textBoxRecherche_Enter);
            this.textBoxRecherche.Leave += new System.EventHandler(this.textBoxRecherche_Leave);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(8)))), ((int)(((byte)(38)))));
            this.panel2.Location = new System.Drawing.Point(54, 175);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1592, 3);
            this.panel2.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(8)))), ((int)(((byte)(38)))));
            this.panel1.Location = new System.Drawing.Point(54, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1592, 3);
            this.panel1.TabIndex = 12;
            // 
            // btnSauvgarderdansUnexecl
            // 
            this.btnSauvgarderdansUnexecl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSauvgarderdansUnexecl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnSauvgarderdansUnexecl.FlatAppearance.BorderSize = 0;
            this.btnSauvgarderdansUnexecl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSauvgarderdansUnexecl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSauvgarderdansUnexecl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSauvgarderdansUnexecl.Image = global::Gestiondestock.Properties.Resources.Excel_icon;
            this.btnSauvgarderdansUnexecl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSauvgarderdansUnexecl.Location = new System.Drawing.Point(1255, 627);
            this.btnSauvgarderdansUnexecl.Name = "btnSauvgarderdansUnexecl";
            this.btnSauvgarderdansUnexecl.Size = new System.Drawing.Size(391, 53);
            this.btnSauvgarderdansUnexecl.TabIndex = 21;
            this.btnSauvgarderdansUnexecl.Text = "    Sauvgarder dans un execl ";
            this.btnSauvgarderdansUnexecl.UseVisualStyleBackColor = false;
            this.btnSauvgarderdansUnexecl.Click += new System.EventHandler(this.btnSauvgarderdansUnexecl_Click);
            // 
            // btnImprimerToutes
            // 
            this.btnImprimerToutes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnImprimerToutes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnImprimerToutes.FlatAppearance.BorderSize = 0;
            this.btnImprimerToutes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimerToutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimerToutes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnImprimerToutes.Image = global::Gestiondestock.Properties.Resources.Imprimer;
            this.btnImprimerToutes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimerToutes.Location = new System.Drawing.Point(685, 627);
            this.btnImprimerToutes.Name = "btnImprimerToutes";
            this.btnImprimerToutes.Size = new System.Drawing.Size(337, 53);
            this.btnImprimerToutes.TabIndex = 20;
            this.btnImprimerToutes.Text = "Imprimer toutes ";
            this.btnImprimerToutes.UseVisualStyleBackColor = false;
            this.btnImprimerToutes.Click += new System.EventHandler(this.btnModifierClient_Click);
            // 
            // btnImprimerPRcocher
            // 
            this.btnImprimerPRcocher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImprimerPRcocher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnImprimerPRcocher.FlatAppearance.BorderSize = 0;
            this.btnImprimerPRcocher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimerPRcocher.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimerPRcocher.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnImprimerPRcocher.Image = global::Gestiondestock.Properties.Resources.Imprimer;
            this.btnImprimerPRcocher.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimerPRcocher.Location = new System.Drawing.Point(63, 627);
            this.btnImprimerPRcocher.Name = "btnImprimerPRcocher";
            this.btnImprimerPRcocher.Size = new System.Drawing.Size(389, 53);
            this.btnImprimerPRcocher.TabIndex = 19;
            this.btnImprimerPRcocher.Text = "Imprimer PR cocher";
            this.btnImprimerPRcocher.UseVisualStyleBackColor = false;
            this.btnImprimerPRcocher.Click += new System.EventHandler(this.btnImprimerPRcocher_Click);
            // 
            // btnafficherphotoProduit
            // 
            this.btnafficherphotoProduit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnafficherphotoProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnafficherphotoProduit.FlatAppearance.BorderSize = 0;
            this.btnafficherphotoProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnafficherphotoProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnafficherphotoProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnafficherphotoProduit.Image = global::Gestiondestock.Properties.Resources.Pictures_icon;
            this.btnafficherphotoProduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnafficherphotoProduit.Location = new System.Drawing.Point(1287, 13);
            this.btnafficherphotoProduit.Name = "btnafficherphotoProduit";
            this.btnafficherphotoProduit.Size = new System.Drawing.Size(359, 53);
            this.btnafficherphotoProduit.TabIndex = 18;
            this.btnafficherphotoProduit.Text = "Afficher Photo";
            this.btnafficherphotoProduit.UseVisualStyleBackColor = false;
            this.btnafficherphotoProduit.Click += new System.EventHandler(this.btnafficherphotoProduit_Click);
            // 
            // btnsupprimerProduit
            // 
            this.btnsupprimerProduit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnsupprimerProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnsupprimerProduit.FlatAppearance.BorderSize = 0;
            this.btnsupprimerProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsupprimerProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsupprimerProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnsupprimerProduit.Image = global::Gestiondestock.Properties.Resources.Close_2_icon;
            this.btnsupprimerProduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsupprimerProduit.Location = new System.Drawing.Point(892, 13);
            this.btnsupprimerProduit.Name = "btnsupprimerProduit";
            this.btnsupprimerProduit.Size = new System.Drawing.Size(353, 53);
            this.btnsupprimerProduit.TabIndex = 11;
            this.btnsupprimerProduit.Text = "Supprimer";
            this.btnsupprimerProduit.UseVisualStyleBackColor = false;
            this.btnsupprimerProduit.Click += new System.EventHandler(this.btnsupprimerProduit_Click);
            // 
            // btnModifierProduit
            // 
            this.btnModifierProduit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnModifierProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnModifierProduit.FlatAppearance.BorderSize = 0;
            this.btnModifierProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifierProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnModifierProduit.Image = global::Gestiondestock.Properties.Resources.Recycle_iconaaa;
            this.btnModifierProduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModifierProduit.Location = new System.Drawing.Point(499, 13);
            this.btnModifierProduit.Name = "btnModifierProduit";
            this.btnModifierProduit.Size = new System.Drawing.Size(363, 53);
            this.btnModifierProduit.TabIndex = 10;
            this.btnModifierProduit.Text = "Modifier";
            this.btnModifierProduit.UseVisualStyleBackColor = false;
            this.btnModifierProduit.Click += new System.EventHandler(this.btnModifierProduit_Click);
            // 
            // btnAjouterProduit
            // 
            this.btnAjouterProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnAjouterProduit.FlatAppearance.BorderSize = 0;
            this.btnAjouterProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAjouterProduit.Image = global::Gestiondestock.Properties.Resources.Actions_list_add_icon;
            this.btnAjouterProduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAjouterProduit.Location = new System.Drawing.Point(63, 13);
            this.btnAjouterProduit.Name = "btnAjouterProduit";
            this.btnAjouterProduit.Size = new System.Drawing.Size(371, 53);
            this.btnAjouterProduit.TabIndex = 9;
            this.btnAjouterProduit.Text = "Ajouter";
            this.btnAjouterProduit.UseVisualStyleBackColor = false;
            this.btnAjouterProduit.Click += new System.EventHandler(this.btnAjouterProduit_Click);
            // 
            // btnActualiser
            // 
            this.btnActualiser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualiser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnActualiser.FlatAppearance.BorderSize = 0;
            this.btnActualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualiser.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnActualiser.Image = global::Gestiondestock.Properties.Resources.Refresh_icon;
            this.btnActualiser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualiser.Location = new System.Drawing.Point(1358, 101);
            this.btnActualiser.Name = "btnActualiser";
            this.btnActualiser.Size = new System.Drawing.Size(275, 45);
            this.btnActualiser.TabIndex = 22;
            this.btnActualiser.Text = "Atualiser";
            this.btnActualiser.UseVisualStyleBackColor = false;
            this.btnActualiser.Click += new System.EventHandler(this.btnActualiser_Click);
            // 
            // UserProduitControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnActualiser);
            this.Controls.Add(this.btnSauvgarderdansUnexecl);
            this.Controls.Add(this.btnImprimerToutes);
            this.Controls.Add(this.btnImprimerPRcocher);
            this.Controls.Add(this.btnafficherphotoProduit);
            this.Controls.Add(this.dvgProduit);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.textBoxRecherche);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnsupprimerProduit);
            this.Controls.Add(this.btnModifierProduit);
            this.Controls.Add(this.btnAjouterProduit);
            this.Name = "UserProduitControl";
            this.Size = new System.Drawing.Size(1709, 713);
            this.Load += new System.EventHandler(this.UserProduitControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvgProduit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dvgProduit;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxRecherche;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnsupprimerProduit;
        private System.Windows.Forms.Button btnModifierProduit;
        private System.Windows.Forms.Button btnAjouterProduit;
        private System.Windows.Forms.Button btnafficherphotoProduit;
        private System.Windows.Forms.Button btnSauvgarderdansUnexecl;
        private System.Windows.Forms.Button btnImprimerToutes;
        private System.Windows.Forms.Button btnImprimerPRcocher;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Colimn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button btnActualiser;
    }
}
