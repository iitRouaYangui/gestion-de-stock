﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class UserListeCommandeControl : UserControl
    {
        private dbStockContext bd;
        private static UserListeCommandeControl UserCommande;
        public static UserListeCommandeControl Instance
        {
            get
            {
                if (UserCommande == null)
                {
                    UserCommande = new UserListeCommandeControl();
                }
                return UserCommande;
            }
        }
        public UserListeCommandeControl()
        {
            InitializeComponent();
            bd = new dbStockContext();
        }

        private void UserListeCommandeControl_Load(object sender, EventArgs e)
        {

        }

        private void btnAjouterProduit_Click(object sender, EventArgs e)
        {
            FRM_Details_Commandes frmdc = new FRM_Details_Commandes();
            frmdc.ShowDialog();
           
        }
    }
}
