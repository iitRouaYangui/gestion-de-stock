﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class FRM_Ajouter_Modifier_Catégorie : Form
    {
        private UserControl usrCategorie;
        public FRM_Ajouter_Modifier_Catégorie(UserControl usrCat)
        {
            InitializeComponent();
            this.usrCategorie = usrCat;
        }

        private void btnannuller_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void textNomCatégorie_TextChanged(object sender, EventArgs e)
        {

        }

        private void textNomCatégorie_Leave(object sender, EventArgs e)
        {
            if (textNomCatégorie.Text == "")
            {
                textNomCatégorie.Text = "Nom Catégorie";
                textNomCatégorie.ForeColor = Color.Silver;
            }
        }

        private void textNomCatégorie_Enter(object sender, EventArgs e)
        {
            if (textNomCatégorie.Text == "Nom Catégorie")
            {
                textNomCatégorie.Text = "";
                textNomCatégorie.ForeColor = Color.White;
            }
        }
        public int IdSelect;
        private void btnEnregistreProduit_Click(object sender, EventArgs e)
        {
            if (textNomCatégorie.Text == "" || textNomCatégorie.Text == "Nom Catégorie")
            {
                MessageBox.Show("Entrer Le Nom de Catégorie", "Obligatoire", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (LBtitreCatégorie.Text == "Ajouter Catégorie")
                {
                    BL.CLS_Categorie ClCategorie = new BL.CLS_Categorie(); // créer un objet pour stock dans BD
                    if (ClCategorie.Ajouter_Categorie(textNomCatégorie.Text))
                    {
                        MessageBox.Show("Catégorie Ajouter avec succes", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        (usrCategorie as UserListeCatégorieControl).Remplirdatagrid();
                    }
                    else
                    {
                        MessageBox.Show("Catégorie déjà existant", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    BL.CLS_Categorie ClCategorie = new BL.CLS_Categorie();
                    UserListeCatégorieControl user = new UserListeCatégorieControl();
                    DialogResult R = MessageBox.Show("Voulez-vous vraiment modifier ce catégorie", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (R == DialogResult.Yes)
                    {
                        ClCategorie.modifier_Categorie(IdSelect, textNomCatégorie.Text);
                        // pour actualiser  datagrid view
                        (usrCategorie as UserListeCatégorieControl).Remplirdatagrid();
                        MessageBox.Show("catégorie Modifier avec succés", "Modification", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Modification est anullé ", "Modification ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }
    }
}
