﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class FRM_Produit_Commande : Form
    {
        public Form Frmddetail;
        public FRM_Produit_Commande(Form frm)
        {
            InitializeComponent();
            Frmddetail = frm;
        }

        private void FRM_Produit_Commande_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }

        private void textQuantité_TextChanged(object sender, EventArgs e)
        {
            if(textQuantité.Text!="")
            {
                int quantite = int.Parse(textQuantité.Text);
                int prix = int.Parse(Prix.Text);
                if(int.Parse(textQuantité.Text)>int.Parse(stock.Text))
                {
                    MessageBox.Show("il ya seualement " + int.Parse(stock.Text) + " dans stock ", "Stock", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //vider textbox quantite
                    textQuantité.Text = "";
                    textTotale.Text = Prix.Text;
                }
                else
                {
                    textTotale.Text = (quantite * prix).ToString();
                }
            }
            else
            {
                textTotale.Text = Prix.Text;
            }
        }

        private void textRemise_TextChanged(object sender, EventArgs e)
        {
            if(textRemise.Text!="")
            {
                int quantite = int.Parse(textQuantité.Text);
                int prix = int.Parse(Prix.Text);
                int total = quantite * prix;
                int remis = int.Parse(textRemise.Text);
                textTotale.Text = (total - (total * remis / 100)).ToString();
            }
            else
            {
                int quantite = int.Parse(textQuantité.Text);
                int prix = int.Parse(Prix.Text);
                textTotale.Text = (quantite * prix).ToString();
            }
        }

        private void btnEnregistre_Click(object sender, EventArgs e)
        {   // si textbox quantite et textbox remis vide
            int quant, remise;
            if(textQuantité.Text!="")
            {
                quant = int.Parse(textQuantité.Text);
            }
            else
            {
                quant = 1;
            }
            if(textRemise.Text!="")
            {
                remise = int.Parse(textRemise.Text);
            }
            else
            {
                remise = 0;
            }
            // ajouter Produit dans Commande
            BL.D_Commande Detail = new BL.D_Commande
            {
                Id = int.Parse(textId.Text),
                Nom = nom.Text,
                Quantite = quant,
                Prix = Prix.Text,
                Remise = remise.ToString(),
                Totale = textTotale.Text

            };
            // ajouter dans liste 
            if(BL.D_Commande.listeDetial.SingleOrDefault(s=> s.Id == Detail.Id) != null)
            {
                MessageBox.Show("Produit déja existe dans Commande ", "Produit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else 
            {
                BL.D_Commande.listeDetial.Add(Detail);
                (Frmddetail as FRM_Details_Commandes).Actualiser_DetailCommande();
            }

        }
    }
}
