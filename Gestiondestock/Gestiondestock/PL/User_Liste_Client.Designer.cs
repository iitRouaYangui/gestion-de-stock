﻿
namespace Gestiondestock.PL
{
    partial class User_Liste_Client
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnsupprimerClient = new System.Windows.Forms.Button();
            this.btnModifierClient = new System.Windows.Forms.Button();
            this.btnAjouterClient = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxRecherche = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboxrecherche = new System.Windows.Forms.ComboBox();
            this.dvgClient = new System.Windows.Forms.DataGridView();
            this.Colimn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ville = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dvgClient)).BeginInit();
            this.SuspendLayout();
            // 
            // btnsupprimerClient
            // 
            this.btnsupprimerClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsupprimerClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnsupprimerClient.FlatAppearance.BorderSize = 0;
            this.btnsupprimerClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsupprimerClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsupprimerClient.Image = global::Gestiondestock.Properties.Resources.Close_2_icon;
            this.btnsupprimerClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsupprimerClient.Location = new System.Drawing.Point(845, 35);
            this.btnsupprimerClient.Name = "btnsupprimerClient";
            this.btnsupprimerClient.Size = new System.Drawing.Size(320, 53);
            this.btnsupprimerClient.TabIndex = 2;
            this.btnsupprimerClient.Text = "Supprimer";
            this.btnsupprimerClient.UseVisualStyleBackColor = false;
            this.btnsupprimerClient.Click += new System.EventHandler(this.btnsupprimerClient_Click);
            // 
            // btnModifierClient
            // 
            this.btnModifierClient.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnModifierClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnModifierClient.FlatAppearance.BorderSize = 0;
            this.btnModifierClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifierClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierClient.Image = global::Gestiondestock.Properties.Resources.Recycle_iconaaa;
            this.btnModifierClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModifierClient.Location = new System.Drawing.Point(453, 35);
            this.btnModifierClient.Name = "btnModifierClient";
            this.btnModifierClient.Size = new System.Drawing.Size(320, 53);
            this.btnModifierClient.TabIndex = 1;
            this.btnModifierClient.Text = "Modifier";
            this.btnModifierClient.UseVisualStyleBackColor = false;
            this.btnModifierClient.Click += new System.EventHandler(this.btnModifierClient_Click);
            // 
            // btnAjouterClient
            // 
            this.btnAjouterClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnAjouterClient.FlatAppearance.BorderSize = 0;
            this.btnAjouterClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterClient.Image = global::Gestiondestock.Properties.Resources.Actions_list_add_icon;
            this.btnAjouterClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAjouterClient.Location = new System.Drawing.Point(54, 35);
            this.btnAjouterClient.Name = "btnAjouterClient";
            this.btnAjouterClient.Size = new System.Drawing.Size(320, 53);
            this.btnAjouterClient.TabIndex = 0;
            this.btnAjouterClient.Text = "Ajouter";
            this.btnAjouterClient.UseVisualStyleBackColor = false;
            this.btnAjouterClient.Click += new System.EventHandler(this.btnAjouterClient_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(8)))), ((int)(((byte)(38)))));
            this.panel1.Location = new System.Drawing.Point(54, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1113, 3);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(8)))), ((int)(((byte)(38)))));
            this.panel2.Location = new System.Drawing.Point(54, 193);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1113, 3);
            this.panel2.TabIndex = 4;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // textBoxRecherche
            // 
            this.textBoxRecherche.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxRecherche.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRecherche.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRecherche.ForeColor = System.Drawing.Color.DimGray;
            this.textBoxRecherche.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxRecherche.Location = new System.Drawing.Point(604, 120);
            this.textBoxRecherche.Multiline = true;
            this.textBoxRecherche.Name = "textBoxRecherche";
            this.textBoxRecherche.Size = new System.Drawing.Size(334, 39);
            this.textBoxRecherche.TabIndex = 5;
            this.textBoxRecherche.Text = "Recherche";
            this.textBoxRecherche.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxRecherche.TextChanged += new System.EventHandler(this.textBoxRecherche_TextChanged);
            this.textBoxRecherche.Enter += new System.EventHandler(this.textBoxRecherche_Enter);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.panel3.Location = new System.Drawing.Point(604, 162);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(334, 3);
            this.panel3.TabIndex = 6;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // comboxrecherche
            // 
            this.comboxrecherche.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboxrecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboxrecherche.FormattingEnabled = true;
            this.comboxrecherche.Items.AddRange(new object[] {
            "Nom",
            "Prenom",
            "Telephone",
            "Email",
            "Ville",
            "Pays"});
            this.comboxrecherche.Location = new System.Drawing.Point(231, 120);
            this.comboxrecherche.Name = "comboxrecherche";
            this.comboxrecherche.Size = new System.Drawing.Size(329, 37);
            this.comboxrecherche.TabIndex = 7;
            this.comboxrecherche.SelectedIndexChanged += new System.EventHandler(this.comboxrecherche_SelectedIndexChanged);
            // 
            // dvgClient
            // 
            this.dvgClient.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvgClient.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvgClient.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dvgClient.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgClient.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvgClient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgClient.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Colimn1,
            this.id,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Ville,
            this.Pays});
            this.dvgClient.EnableHeadersVisualStyles = false;
            this.dvgClient.Location = new System.Drawing.Point(0, 202);
            this.dvgClient.Name = "dvgClient";
            this.dvgClient.RowHeadersVisible = false;
            this.dvgClient.RowHeadersWidth = 51;
            this.dvgClient.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dvgClient.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dvgClient.RowTemplate.Height = 24;
            this.dvgClient.Size = new System.Drawing.Size(1230, 511);
            this.dvgClient.TabIndex = 8;
            this.dvgClient.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgClient_CellContentClick);
            // 
            // Colimn1
            // 
            this.Colimn1.HeaderText = "Select";
            this.Colimn1.MinimumWidth = 6;
            this.Colimn1.Name = "Colimn1";
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nom";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Prenon";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Adresse";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Telephone";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Email";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Ville
            // 
            this.Ville.HeaderText = "Ville";
            this.Ville.MinimumWidth = 6;
            this.Ville.Name = "Ville";
            this.Ville.ReadOnly = true;
            // 
            // Pays
            // 
            this.Pays.HeaderText = "Pays";
            this.Pays.MinimumWidth = 6;
            this.Pays.Name = "Pays";
            this.Pays.ReadOnly = true;
            // 
            // User_Liste_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.dvgClient);
            this.Controls.Add(this.comboxrecherche);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.textBoxRecherche);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnsupprimerClient);
            this.Controls.Add(this.btnModifierClient);
            this.Controls.Add(this.btnAjouterClient);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "User_Liste_Client";
            this.Size = new System.Drawing.Size(1230, 713);
            this.Load += new System.EventHandler(this.User_Liste_Client_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvgClient)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAjouterClient;
        private System.Windows.Forms.Button btnModifierClient;
        private System.Windows.Forms.Button btnsupprimerClient;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxRecherche;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboxrecherche;
        private System.Windows.Forms.DataGridView dvgClient;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Colimn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ville;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pays;
    }
}
