﻿
namespace Gestiondestock.PL
{
    partial class UserListeCatégorieControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxRecherche = new System.Windows.Forms.TextBox();
            this.btnAjouterProduit = new System.Windows.Forms.Button();
            this.DvgCatégorie = new System.Windows.Forms.DataGridView();
            this.Select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modifier = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Supprimer = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnImprimerToutes = new System.Windows.Forms.Button();
            this.btnSauvgarderdansUnexecl = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DvgCatégorie)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.panel3.Location = new System.Drawing.Point(452, 55);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(728, 3);
            this.panel3.TabIndex = 17;
            // 
            // textBoxRecherche
            // 
            this.textBoxRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRecherche.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxRecherche.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRecherche.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRecherche.ForeColor = System.Drawing.Color.DimGray;
            this.textBoxRecherche.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxRecherche.Location = new System.Drawing.Point(452, 13);
            this.textBoxRecherche.Multiline = true;
            this.textBoxRecherche.Name = "textBoxRecherche";
            this.textBoxRecherche.Size = new System.Drawing.Size(728, 39);
            this.textBoxRecherche.TabIndex = 16;
            this.textBoxRecherche.Text = "Recherche";
            this.textBoxRecherche.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxRecherche.TextChanged += new System.EventHandler(this.textBoxRecherche_TextChanged);
            this.textBoxRecherche.Enter += new System.EventHandler(this.textBoxRecherche_Enter);
            this.textBoxRecherche.Leave += new System.EventHandler(this.textBoxRecherche_Leave);
            // 
            // btnAjouterProduit
            // 
            this.btnAjouterProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnAjouterProduit.FlatAppearance.BorderSize = 0;
            this.btnAjouterProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAjouterProduit.Image = global::Gestiondestock.Properties.Resources.Actions_list_add_icon;
            this.btnAjouterProduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAjouterProduit.Location = new System.Drawing.Point(42, 8);
            this.btnAjouterProduit.Name = "btnAjouterProduit";
            this.btnAjouterProduit.Size = new System.Drawing.Size(297, 53);
            this.btnAjouterProduit.TabIndex = 18;
            this.btnAjouterProduit.Text = "Ajouter";
            this.btnAjouterProduit.UseVisualStyleBackColor = false;
            this.btnAjouterProduit.Click += new System.EventHandler(this.btnAjouterProduit_Click);
            // 
            // DvgCatégorie
            // 
            this.DvgCatégorie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DvgCatégorie.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DvgCatégorie.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DvgCatégorie.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DvgCatégorie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DvgCatégorie.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.Id,
            this.Column1,
            this.Modifier,
            this.Supprimer});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DvgCatégorie.DefaultCellStyle = dataGridViewCellStyle4;
            this.DvgCatégorie.EnableHeadersVisualStyles = false;
            this.DvgCatégorie.Location = new System.Drawing.Point(0, 116);
            this.DvgCatégorie.Name = "DvgCatégorie";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DvgCatégorie.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DvgCatégorie.RowHeadersVisible = false;
            this.DvgCatégorie.RowHeadersWidth = 51;
            this.DvgCatégorie.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.DvgCatégorie.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DvgCatégorie.RowTemplate.Height = 24;
            this.DvgCatégorie.Size = new System.Drawing.Size(1191, 489);
            this.DvgCatégorie.TabIndex = 19;
            this.DvgCatégorie.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DvgCatégorie_CellContentClick);
            // 
            // Select
            // 
            this.Select.HeaderText = "Select";
            this.Select.MinimumWidth = 6;
            this.Select.Name = "Select";
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nom Catégorie";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // Modifier
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Modifier.DefaultCellStyle = dataGridViewCellStyle2;
            this.Modifier.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Modifier.HeaderText = "Modifier";
            this.Modifier.MinimumWidth = 6;
            this.Modifier.Name = "Modifier";
            this.Modifier.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Modifier.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Modifier.Text = "Modifier";
            this.Modifier.UseColumnTextForButtonValue = true;
            // 
            // Supprimer
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Supprimer.DefaultCellStyle = dataGridViewCellStyle3;
            this.Supprimer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Supprimer.HeaderText = "Supprimer";
            this.Supprimer.MinimumWidth = 6;
            this.Supprimer.Name = "Supprimer";
            this.Supprimer.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Supprimer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Supprimer.Text = "Supprimer";
            this.Supprimer.UseColumnTextForButtonValue = true;
            // 
            // btnImprimerToutes
            // 
            this.btnImprimerToutes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImprimerToutes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnImprimerToutes.FlatAppearance.BorderSize = 0;
            this.btnImprimerToutes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimerToutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimerToutes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnImprimerToutes.Image = global::Gestiondestock.Properties.Resources.Imprimer;
            this.btnImprimerToutes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimerToutes.Location = new System.Drawing.Point(151, 655);
            this.btnImprimerToutes.Name = "btnImprimerToutes";
            this.btnImprimerToutes.Size = new System.Drawing.Size(385, 53);
            this.btnImprimerToutes.TabIndex = 21;
            this.btnImprimerToutes.Text = "Imprimer toutes ";
            this.btnImprimerToutes.UseVisualStyleBackColor = false;
            this.btnImprimerToutes.Click += new System.EventHandler(this.btnImprimerToutes_Click);
            // 
            // btnSauvgarderdansUnexecl
            // 
            this.btnSauvgarderdansUnexecl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSauvgarderdansUnexecl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.btnSauvgarderdansUnexecl.FlatAppearance.BorderSize = 0;
            this.btnSauvgarderdansUnexecl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSauvgarderdansUnexecl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSauvgarderdansUnexecl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSauvgarderdansUnexecl.Image = global::Gestiondestock.Properties.Resources.Excel_icon;
            this.btnSauvgarderdansUnexecl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSauvgarderdansUnexecl.Location = new System.Drawing.Point(707, 655);
            this.btnSauvgarderdansUnexecl.Name = "btnSauvgarderdansUnexecl";
            this.btnSauvgarderdansUnexecl.Size = new System.Drawing.Size(385, 53);
            this.btnSauvgarderdansUnexecl.TabIndex = 22;
            this.btnSauvgarderdansUnexecl.Text = "    Sauvgarder dans un execl ";
            this.btnSauvgarderdansUnexecl.UseVisualStyleBackColor = false;
            this.btnSauvgarderdansUnexecl.Click += new System.EventHandler(this.btnSauvgarderdansUnexecl_Click);
            // 
            // UserListeCatégorieControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSauvgarderdansUnexecl);
            this.Controls.Add(this.btnImprimerToutes);
            this.Controls.Add(this.DvgCatégorie);
            this.Controls.Add(this.btnAjouterProduit);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.textBoxRecherche);
            this.Name = "UserListeCatégorieControl";
            this.Size = new System.Drawing.Size(1194, 745);
            this.Load += new System.EventHandler(this.UserListeCatégorieControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DvgCatégorie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxRecherche;
        private System.Windows.Forms.Button btnAjouterProduit;
        private System.Windows.Forms.DataGridView DvgCatégorie;
        private System.Windows.Forms.Button btnImprimerToutes;
        private System.Windows.Forms.Button btnSauvgarderdansUnexecl;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewButtonColumn Modifier;
        private System.Windows.Forms.DataGridViewButtonColumn Supprimer;
    }
}
