﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class FRM_Details_Commandes : Form
    {
        dbStockContext bd = new dbStockContext();
        public FRM_Details_Commandes()
        {
            InitializeComponent();
        }
        // Remplir datagrid de commande par liste
        public void Actualiser_DetailCommande()
        {
            dataGridCommande.Rows.Clear();
            foreach(var L in BL.D_Commande.listeDetial)
            {
                dataGridCommande.Rows.Add(L.Id, L.Nom, L.Quantite, L.Prix, L.Remise, L.Totale);
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBoxRecherche_Enter(object sender, EventArgs e)
        {

            if (textBoxRecherche.Text == "Recherche")
            {
                textBoxRecherche.Text = "";
                textBoxRecherche.ForeColor = Color.White;
            }
        }

        private void textBoxRecherche_Leave(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "")
            {
                textBoxRecherche.Text = "Recherche";
                textBoxRecherche.ForeColor = Color.Silver;
            }
        }
        private void textBoxRecherche_TextChanged(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            
            var listerecherche = bd.Produit.ToList(); // liste recherche = liste des produit
            // recherche suelement par nom de produit
            if (textBoxRecherche.Text != "Recherche")
            {
                listerecherche = listerecherche.Where(s => s.Nom_Produit.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                //vider data grid 
                dvgProduit.Rows.Clear();
                // ajouter listerecherche dans la datagrid clien
                foreach (var l in listerecherche)
                {
                    dvgProduit.Rows.Add( l.Id_Produit, l.Nom_Produit, l.Quantite_Produit, l.Prix_Produit);
                }
                for (int i = 0; i < dvgProduit.Rows.Count; i++)
                {
                    object value = dvgProduit.Rows[i].Cells[2].Value;
                    if (value != null && (int)value==0)
                    {
                        dvgProduit.Rows[i].Cells[2].Style.BackColor = Color.Red;

                    }
                    else
                    {
                        dvgProduit.Rows[i].Cells[2].Style.BackColor = Color.LightGreen;
                    }
                }
                dvgProduit.ClearSelection();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FRM_Client_Commande frmcc = new FRM_Client_Commande();
            frmcc.ShowDialog();
            textNomClient.Text = frmcc.dvgClient.CurrentRow.Cells[1].Value.ToString();
            textPrenomClient.Text= frmcc.dvgClient.CurrentRow.Cells[2].Value.ToString();
            textTelephoneCLient.Text = frmcc.dvgClient.CurrentRow.Cells[4].Value.ToString();
            textEmailClient.Text= frmcc.dvgClient.CurrentRow.Cells[5].Value.ToString();
            textVilleCLient.Text= frmcc.dvgClient.CurrentRow.Cells[6].Value.ToString();
            textPaysClient.Text= frmcc.dvgClient.CurrentRow.Cells[7].Value.ToString();
        }

        private void dvgProduit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dvgProduit_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {   
            FRM_Produit_Commande frmpc = new FRM_Produit_Commande(this);
            object value = dvgProduit.CurrentRow.Cells[2].Value;
            if (value != null && (int)value == 0)
            {
                MessageBox.Show("Stock Vide", "Stock", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {   frmpc.textId.Text = dvgProduit.CurrentRow.Cells[0].Value.ToString();
                frmpc.nom.Text = dvgProduit.CurrentRow.Cells[1].Value.ToString();
                frmpc.stock.Text = dvgProduit.CurrentRow.Cells[2].Value.ToString();
                frmpc.Prix.Text = dvgProduit.CurrentRow.Cells[3].Value.ToString();
                frmpc.textTotale.Text= dvgProduit.CurrentRow.Cells[3].Value.ToString();
                frmpc.ShowDialog();
               
            }
            
        }
    }
}
