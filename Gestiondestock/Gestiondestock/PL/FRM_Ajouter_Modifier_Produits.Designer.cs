﻿
namespace Gestiondestock.PL
{
    partial class FRM_Ajouter_Modifier_Produits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LBtitreProduit = new System.Windows.Forms.Label();
            this.Image = new System.Windows.Forms.Label();
            this.Catégorie = new System.Windows.Forms.Label();
            this.comboxrecherche = new System.Windows.Forms.ComboBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textPrix = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textQTE = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textPrenomProduit = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnEnregistreProduit = new System.Windows.Forms.Button();
            this.btnActualiserProduit = new System.Windows.Forms.Button();
            this.btnParcourire = new System.Windows.Forms.Button();
            this.btnannuller = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).BeginInit();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(862, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(3, 541);
            this.panel4.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 541);
            this.panel3.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 544);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(865, 3);
            this.panel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(865, 3);
            this.panel1.TabIndex = 4;
            // 
            // LBtitreProduit
            // 
            this.LBtitreProduit.AutoSize = true;
            this.LBtitreProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitreProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBtitreProduit.Location = new System.Drawing.Point(285, 19);
            this.LBtitreProduit.Name = "LBtitreProduit";
            this.LBtitreProduit.Size = new System.Drawing.Size(287, 44);
            this.LBtitreProduit.TabIndex = 8;
            this.LBtitreProduit.Text = "Ajouter Produit";
            // 
            // Image
            // 
            this.Image.AutoSize = true;
            this.Image.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Image.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Image.Location = new System.Drawing.Point(28, 99);
            this.Image.Name = "Image";
            this.Image.Size = new System.Drawing.Size(89, 24);
            this.Image.TabIndex = 10;
            this.Image.Text = "Image :";
            // 
            // Catégorie
            // 
            this.Catégorie.AutoSize = true;
            this.Catégorie.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Catégorie.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Catégorie.Location = new System.Drawing.Point(490, 99);
            this.Catégorie.Name = "Catégorie";
            this.Catégorie.Size = new System.Drawing.Size(120, 24);
            this.Catégorie.TabIndex = 11;
            this.Catégorie.Text = "Catégorie :";
            // 
            // comboxrecherche
            // 
            this.comboxrecherche.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboxrecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboxrecherche.FormattingEnabled = true;
            this.comboxrecherche.Items.AddRange(new object[] {
            "",
            "Nom",
            "Quantite",
            "prix "});
            this.comboxrecherche.Location = new System.Drawing.Point(610, 92);
            this.comboxrecherche.Name = "comboxrecherche";
            this.comboxrecherche.Size = new System.Drawing.Size(243, 37);
            this.comboxrecherche.TabIndex = 17;
            this.comboxrecherche.SelectedIndexChanged += new System.EventHandler(this.comboxrecherche_SelectedIndexChanged);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel8.Location = new System.Drawing.Point(568, 331);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(250, 1);
            this.panel8.TabIndex = 26;
            // 
            // textPrix
            // 
            this.textPrix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textPrix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrix.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textPrix.Location = new System.Drawing.Point(563, 295);
            this.textPrix.Multiline = true;
            this.textPrix.Name = "textPrix";
            this.textPrix.Size = new System.Drawing.Size(272, 38);
            this.textPrix.TabIndex = 24;
            this.textPrix.Text = "Prix";
            this.textPrix.Enter += new System.EventHandler(this.textPrix_Enter);
            this.textPrix.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textPrix_KeyPress);
            this.textPrix.Leave += new System.EventHandler(this.textPrix_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel7.Location = new System.Drawing.Point(568, 257);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(250, 1);
            this.panel7.TabIndex = 23;
            // 
            // textQTE
            // 
            this.textQTE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textQTE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textQTE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textQTE.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textQTE.Location = new System.Drawing.Point(563, 221);
            this.textQTE.Multiline = true;
            this.textQTE.Name = "textQTE";
            this.textQTE.Size = new System.Drawing.Size(272, 38);
            this.textQTE.TabIndex = 21;
            this.textQTE.Text = "Quantite";
            this.textQTE.Enter += new System.EventHandler(this.textQTE_Enter);
            this.textQTE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textQTE_KeyPress);
            this.textQTE.Leave += new System.EventHandler(this.textQTE_Leave);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel6.Location = new System.Drawing.Point(568, 186);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(250, 1);
            this.panel6.TabIndex = 20;
            // 
            // textPrenomProduit
            // 
            this.textPrenomProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textPrenomProduit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPrenomProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrenomProduit.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textPrenomProduit.Location = new System.Drawing.Point(563, 146);
            this.textPrenomProduit.Multiline = true;
            this.textPrenomProduit.Name = "textPrenomProduit";
            this.textPrenomProduit.Size = new System.Drawing.Size(272, 38);
            this.textPrenomProduit.TabIndex = 18;
            this.textPrenomProduit.Text = "Nom Produit";
            this.textPrenomProduit.TextChanged += new System.EventHandler(this.textPrenomProduit_TextChanged);
            this.textPrenomProduit.Enter += new System.EventHandler(this.textPrenomProduit_Enter);
            this.textPrenomProduit.Leave += new System.EventHandler(this.textPrenomProduit_Leave);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Gestiondestock.Properties.Resources.prix_32;
            this.pictureBox5.Location = new System.Drawing.Point(518, 295);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(39, 38);
            this.pictureBox5.TabIndex = 25;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Gestiondestock.Properties.Resources.Quantie_32;
            this.pictureBox4.Location = new System.Drawing.Point(518, 221);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 38);
            this.pictureBox4.TabIndex = 22;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Gestiondestock.Properties.Resources.product_32;
            this.pictureBox3.Location = new System.Drawing.Point(518, 150);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 38);
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox1.Location = new System.Drawing.Point(128, 99);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(341, 258);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnEnregistreProduit
            // 
            this.btnEnregistreProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnEnregistreProduit.FlatAppearance.BorderSize = 0;
            this.btnEnregistreProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistreProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistreProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEnregistreProduit.Location = new System.Drawing.Point(494, 460);
            this.btnEnregistreProduit.Name = "btnEnregistreProduit";
            this.btnEnregistreProduit.Size = new System.Drawing.Size(255, 55);
            this.btnEnregistreProduit.TabIndex = 32;
            this.btnEnregistreProduit.Text = "Enregistre ";
            this.btnEnregistreProduit.UseVisualStyleBackColor = false;
            this.btnEnregistreProduit.Click += new System.EventHandler(this.btnEnregistreProduit_Click);
            // 
            // btnActualiserProduit
            // 
            this.btnActualiserProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnActualiserProduit.FlatAppearance.BorderSize = 0;
            this.btnActualiserProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualiserProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualiserProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnActualiserProduit.Location = new System.Drawing.Point(153, 460);
            this.btnActualiserProduit.Name = "btnActualiserProduit";
            this.btnActualiserProduit.Size = new System.Drawing.Size(257, 55);
            this.btnActualiserProduit.TabIndex = 31;
            this.btnActualiserProduit.Text = "Actualiser";
            this.btnActualiserProduit.UseVisualStyleBackColor = false;
            this.btnActualiserProduit.Click += new System.EventHandler(this.btnActualiserProduit_Click);
            // 
            // btnParcourire
            // 
            this.btnParcourire.BackColor = System.Drawing.Color.SteelBlue;
            this.btnParcourire.FlatAppearance.BorderSize = 0;
            this.btnParcourire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParcourire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParcourire.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnParcourire.Location = new System.Drawing.Point(278, 379);
            this.btnParcourire.Name = "btnParcourire";
            this.btnParcourire.Size = new System.Drawing.Size(191, 48);
            this.btnParcourire.TabIndex = 33;
            this.btnParcourire.Text = "Parcourire...";
            this.btnParcourire.UseVisualStyleBackColor = false;
            this.btnParcourire.Click += new System.EventHandler(this.btnParcourire_Click);
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::Gestiondestock.Properties.Resources.Button_Delete_icon;
            this.btnannuller.Location = new System.Drawing.Point(817, 3);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(39, 38);
            this.btnannuller.TabIndex = 34;
            this.btnannuller.TabStop = false;
            this.btnannuller.Click += new System.EventHandler(this.btnannuller_Click);
            // 
            // FRM_Ajouter_Modifier_Produits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(865, 547);
            this.Controls.Add(this.btnannuller);
            this.Controls.Add(this.btnParcourire);
            this.Controls.Add(this.btnEnregistreProduit);
            this.Controls.Add(this.btnActualiserProduit);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.textPrix);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.textQTE);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textPrenomProduit);
            this.Controls.Add(this.comboxrecherche);
            this.Controls.Add(this.Catégorie);
            this.Controls.Add(this.Image);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LBtitreProduit);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRM_Ajouter_Modifier_Produits";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_Ajouter_Modifier_Produits";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label LBtitreProduit;
        private System.Windows.Forms.Label Image;
        private System.Windows.Forms.Label Catégorie;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.TextBox textPrix;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.TextBox textQTE;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.TextBox textPrenomProduit;
        private System.Windows.Forms.Button btnEnregistreProduit;
        public System.Windows.Forms.Button btnActualiserProduit;
        private System.Windows.Forms.Button btnParcourire;
        private System.Windows.Forms.PictureBox btnannuller;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.ComboBox comboxrecherche;
    }
}