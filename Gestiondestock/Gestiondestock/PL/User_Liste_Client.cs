﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class User_Liste_Client : UserControl
    {
        private dbStockContext bd;
        private static User_Liste_Client UserClient;
        // Creer un instance controle 
        public static User_Liste_Client Instance
        {
            get
            {
                if (UserClient == null)
                {
                    UserClient = new User_Liste_Client();
                }
                return UserClient;
            }
        }
        public User_Liste_Client()
        {
            InitializeComponent();
            bd = new dbStockContext();
            // desactiver textbox de recherche 
            textBoxRecherche.Enabled = false;
        }
        // Ajouter dans datagridview 
        public void Actualiserdatagrid()
        {
            bd = new dbStockContext();
            dvgClient.Rows.Clear(); // valider datagrid view 
            foreach (var S in bd.Client)
            {   // ajouter les lignes dans datagrid 
                dvgClient.Rows.Add(false, S.ID_Client, S.Nom_client, S.Prenom_Client, S.Adresse_Client, S.telephone_Client, S.Email_Client, S.Ville_CLient, S.Pays_Client);
            }
        }
        // verif combien de ligne selectionner
        public string SelectVerif()
        {
            int Nombreligneselect = 0;
            for (int i = 0; i < dvgClient.Rows.Count; i++)
            {
                object value = dvgClient.Rows[i].Cells[0].Value;
                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    Nombreligneselect++; // nombreligneselet +1 

                }
            }
            if (Nombreligneselect == 0)
            {
                return " Selectionner le client que vous-voulez modifier ";

            }
            if (Nombreligneselect > 1)
            {
                return " Selectionner seulement 1 seul client pour modifier ";

            }
            return null;

        }
        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBoxRecherche_Enter(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "Recherche")
            {
                textBoxRecherche.Text = "";
                textBoxRecherche.ForeColor = Color.Black;
            }
        }

        private void User_Liste_Client_Load(object sender, EventArgs e)
        {

            Actualiserdatagrid();


        }
        // pour afficher la FRM ajouter modifier client btn ajoute 
        private void btnAjouterClient_Click(object sender, EventArgs e)
        {
            //afficher le formaulaire de saisir 
            PL.FRM_Ajoute_Modifier_client frmclient = new FRM_Ajoute_Modifier_client(this);
            frmclient.ShowDialog();
        }
        // pour afficher la FRM ajouter modifier client btn modifier
        private void btnModifierClient_Click(object sender, EventArgs e)
        {
            PL.FRM_Ajoute_Modifier_client frmclient = new PL.FRM_Ajoute_Modifier_client(this);
            if (SelectVerif() == null)
            {
                for (int i = 0; i < dvgClient.Rows.Count; i++)
                {
                    object value = dvgClient.Rows[i].Cells[0].Value;
                    if (value != null && (Boolean)value) // si le ligne est selectionner 
                    {
                        frmclient.IdSelect = (int)dvgClient.Rows[i].Cells[1].Value;
                        frmclient.textNomClient.Text = dvgClient.Rows[i].Cells[2].Value.ToString();
                        frmclient.textPrenomClient.Text = dvgClient.Rows[i].Cells[3].Value.ToString();
                        frmclient.textAdresseClient.Text = dvgClient.Rows[i].Cells[4].Value.ToString();
                        frmclient.textTELCLIENT.Text = dvgClient.Rows[i].Cells[5].Value.ToString();
                        frmclient.textEmailClient.Text = dvgClient.Rows[i].Cells[6].Value.ToString();
                        frmclient.textVilleClient.Text = dvgClient.Rows[i].Cells[7].Value.ToString();
                        frmclient.textPaysClient.Text = dvgClient.Rows[i].Cells[8].Value.ToString();

                    }
                }

                frmclient.LBtitre.Text = "Modifier Client ";
                frmclient.btnActualiser.Visible = false;
                frmclient.ShowDialog();
            }
            else
            {
                MessageBox.Show(SelectVerif(), "Modifier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnsupprimerClient_Click(object sender, EventArgs e)
        {
            BL.CLS_Client ClClient = new BL.CLS_Client();
            int select = 0;
            // pour supprimr tous les client selectionner 
            for (int i = 0; i < dvgClient.Rows.Count; i++)
            {
                object value = dvgClient.Rows[i].Cells[0].Value;
                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    select++;
                }
            }
                if (select == 0)
                {
                    MessageBox.Show("aucun client selectionner", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                }
                else
                {
                    DialogResult R = MessageBox.Show("Voulez-vous vraiment supprimer ce client", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (R == DialogResult.Yes)
                    {   //Pour supprimer toutes les client selectionner
                        for (int j = 0; j < dvgClient.Rows.Count; j++)
                        {
                            object val = dvgClient.Rows[j].Cells[0].Value;
                            if (val != null && (Boolean)val) // si le ligne est selectionner 
                            {
                                ClClient.suprimer_Client(int.Parse(dvgClient.Rows[j].Cells[1].Value.ToString()));
                            }
                        }
                        // actualiser datagrod view
                        Actualiserdatagrid();
                        MessageBox.Show("Suppresion avec succées ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }





                 }

             
         }

       

        private void comboxrecherche_SelectedIndexChanged(object sender, EventArgs e)
        {
            // activer le textbox recherche si j'ai choisie un champ
            textBoxRecherche.Enabled = true;
            textBoxRecherche.Text = "";
        }
        private void textBoxRecherche_TextChanged(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            var listerecherche = bd.Client.ToList(); // liste des clients 
            if (textBoxRecherche.Text != "") // pas vide
            {
                switch (comboxrecherche.Text)
                {
                    case "Nom":
                        listerecherche=listerecherche.Where(s => s.Nom_client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        // stringcomparison.currentcultureignorecase = sois j'ai écrit le premier lettre en maj o minsc
                        // !=-1 existe dans la base de donnée 
                        break;
                    case "Prenom":
                        listerecherche = listerecherche.Where(s => s.Prenom_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();

                        break;
                    case "Adresse":
                        listerecherche = listerecherche.Where(s => s.Adresse_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    case "Telephone":
                        listerecherche = listerecherche.Where(s => s.telephone_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    case "Email":
                        listerecherche = listerecherche.Where(s => s.Email_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    case "Ville":
                        listerecherche = listerecherche.Where(s => s.Ville_CLient.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    case "Pays":
                        listerecherche = listerecherche.Where(s => s.Pays_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;


                }

            }
            //vider data grid 
            dvgClient.Rows.Clear();
            // ajouter listerecherche dans la datagrid clien
            foreach(var l in listerecherche)
            {
                dvgClient.Rows.Add(false, l.ID_Client,l.Nom_client,l.Prenom_Client,l.Adresse_Client,l.telephone_Client,l.Email_Client, l.Ville_CLient, l.Pays_Client);
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dvgClient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }   }
