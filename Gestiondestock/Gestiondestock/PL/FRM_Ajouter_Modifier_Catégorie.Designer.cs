﻿
namespace Gestiondestock.PL
{
    partial class FRM_Ajouter_Modifier_Catégorie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBtitreCatégorie = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textNomCatégorie = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnannuller = new System.Windows.Forms.PictureBox();
            this.btnEnregistreProduit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).BeginInit();
            this.SuspendLayout();
            // 
            // LBtitreCatégorie
            // 
            this.LBtitreCatégorie.AutoSize = true;
            this.LBtitreCatégorie.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitreCatégorie.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LBtitreCatégorie.Location = new System.Drawing.Point(19, 23);
            this.LBtitreCatégorie.Name = "LBtitreCatégorie";
            this.LBtitreCatégorie.Size = new System.Drawing.Size(264, 36);
            this.LBtitreCatégorie.TabIndex = 39;
            this.LBtitreCatégorie.Text = "Ajouter Catégorie";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(417, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(3, 290);
            this.panel4.TabIndex = 38;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 290);
            this.panel3.TabIndex = 37;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 293);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(420, 3);
            this.panel2.TabIndex = 36;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 3);
            this.panel1.TabIndex = 35;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel6.Location = new System.Drawing.Point(77, 150);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(294, 1);
            this.panel6.TabIndex = 43;
            // 
            // textNomCatégorie
            // 
            this.textNomCatégorie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.textNomCatégorie.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNomCatégorie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomCatégorie.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.textNomCatégorie.Location = new System.Drawing.Point(72, 110);
            this.textNomCatégorie.Multiline = true;
            this.textNomCatégorie.Name = "textNomCatégorie";
            this.textNomCatégorie.Size = new System.Drawing.Size(316, 38);
            this.textNomCatégorie.TabIndex = 41;
            this.textNomCatégorie.Text = "Nom Catégorie";
            this.textNomCatégorie.TextChanged += new System.EventHandler(this.textNomCatégorie_TextChanged);
            this.textNomCatégorie.Enter += new System.EventHandler(this.textNomCatégorie_Enter);
            this.textNomCatégorie.Leave += new System.EventHandler(this.textNomCatégorie_Leave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Gestiondestock.Properties.Resources.Categorize_32;
            this.pictureBox3.Location = new System.Drawing.Point(27, 111);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 38);
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::Gestiondestock.Properties.Resources.Button_Delete_icon;
            this.btnannuller.Location = new System.Drawing.Point(373, 9);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(39, 38);
            this.btnannuller.TabIndex = 40;
            this.btnannuller.TabStop = false;
            this.btnannuller.Click += new System.EventHandler(this.btnannuller_Click);
            // 
            // btnEnregistreProduit
            // 
            this.btnEnregistreProduit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnEnregistreProduit.FlatAppearance.BorderSize = 0;
            this.btnEnregistreProduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistreProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistreProduit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEnregistreProduit.Location = new System.Drawing.Point(72, 203);
            this.btnEnregistreProduit.Name = "btnEnregistreProduit";
            this.btnEnregistreProduit.Size = new System.Drawing.Size(255, 55);
            this.btnEnregistreProduit.TabIndex = 44;
            this.btnEnregistreProduit.Text = "Enregistre ";
            this.btnEnregistreProduit.UseVisualStyleBackColor = false;
            this.btnEnregistreProduit.Click += new System.EventHandler(this.btnEnregistreProduit_Click);
            // 
            // FRM_Ajouter_Modifier_Catégorie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(420, 296);
            this.Controls.Add(this.btnEnregistreProduit);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textNomCatégorie);
            this.Controls.Add(this.btnannuller);
            this.Controls.Add(this.LBtitreCatégorie);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRM_Ajouter_Modifier_Catégorie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_Ajouter_Modifier_Catégorie";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btnannuller;
        public System.Windows.Forms.Label LBtitreCatégorie;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.TextBox textNomCatégorie;
        private System.Windows.Forms.Button btnEnregistreProduit;
    }
}