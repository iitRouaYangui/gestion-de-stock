﻿
namespace Gestiondestock.PL
{
    partial class FRM_Produit_Commande
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textTotale = new System.Windows.Forms.TextBox();
            this.textRemise = new System.Windows.Forms.TextBox();
            this.textQuantité = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textId = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.Label();
            this.Prix = new System.Windows.Forms.Label();
            this.stock = new System.Windows.Forms.Label();
            this.nom = new System.Windows.Forms.Label();
            this.lbPrix = new System.Windows.Forms.Label();
            this.lbStock = new System.Windows.Forms.Label();
            this.LbNom = new System.Windows.Forms.Label();
            this.btnEnregistre = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(497, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(3, 485);
            this.panel4.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 485);
            this.panel3.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 488);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(500, 3);
            this.panel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 3);
            this.panel1.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textTotale);
            this.groupBox1.Controls.Add(this.textRemise);
            this.groupBox1.Controls.Add(this.textQuantité);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(34, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 343);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vendre Produit";
            // 
            // textTotale
            // 
            this.textTotale.Enabled = false;
            this.textTotale.Location = new System.Drawing.Point(266, 255);
            this.textTotale.Name = "textTotale";
            this.textTotale.Size = new System.Drawing.Size(161, 30);
            this.textTotale.TabIndex = 7;
            // 
            // textRemise
            // 
            this.textRemise.Location = new System.Drawing.Point(266, 166);
            this.textRemise.Name = "textRemise";
            this.textRemise.Size = new System.Drawing.Size(161, 30);
            this.textRemise.TabIndex = 6;
            this.textRemise.TextChanged += new System.EventHandler(this.textRemise_TextChanged);
            this.textRemise.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // textQuantité
            // 
            this.textQuantité.Location = new System.Drawing.Point(266, 79);
            this.textQuantité.Name = "textQuantité";
            this.textQuantité.Size = new System.Drawing.Size(161, 30);
            this.textQuantité.TabIndex = 5;
            this.textQuantité.TextChanged += new System.EventHandler(this.textQuantité_TextChanged);
            this.textQuantité.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 25);
            this.label4.TabIndex = 4;
            this.label4.Text = "--------------------";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "--Tolale----------";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "--Remise---------";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(261, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "--Quantité-------";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textId);
            this.groupBox2.Controls.Add(this.id);
            this.groupBox2.Controls.Add(this.Prix);
            this.groupBox2.Controls.Add(this.stock);
            this.groupBox2.Controls.Add(this.nom);
            this.groupBox2.Controls.Add(this.lbPrix);
            this.groupBox2.Controls.Add(this.lbStock);
            this.groupBox2.Controls.Add(this.LbNom);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(17, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(228, 271);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Détail prix";
            // 
            // textId
            // 
            this.textId.AutoSize = true;
            this.textId.Location = new System.Drawing.Point(104, 42);
            this.textId.Name = "textId";
            this.textId.Size = new System.Drawing.Size(70, 25);
            this.textId.TabIndex = 7;
            this.textId.Text = "label8";
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.id.Location = new System.Drawing.Point(6, 42);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(43, 25);
            this.id.TabIndex = 6;
            this.id.Text = "Id :";
            // 
            // Prix
            // 
            this.Prix.AutoSize = true;
            this.Prix.Location = new System.Drawing.Point(92, 186);
            this.Prix.Name = "Prix";
            this.Prix.Size = new System.Drawing.Size(82, 25);
            this.Prix.TabIndex = 5;
            this.Prix.Text = "label10";
            // 
            // stock
            // 
            this.stock.AutoSize = true;
            this.stock.Location = new System.Drawing.Point(104, 127);
            this.stock.Name = "stock";
            this.stock.Size = new System.Drawing.Size(70, 25);
            this.stock.TabIndex = 4;
            this.stock.Text = "label9";
            // 
            // nom
            // 
            this.nom.AutoSize = true;
            this.nom.Location = new System.Drawing.Point(104, 80);
            this.nom.Name = "nom";
            this.nom.Size = new System.Drawing.Size(70, 25);
            this.nom.TabIndex = 3;
            this.nom.Text = "label8";
            // 
            // lbPrix
            // 
            this.lbPrix.AutoSize = true;
            this.lbPrix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbPrix.Location = new System.Drawing.Point(6, 186);
            this.lbPrix.Name = "lbPrix";
            this.lbPrix.Size = new System.Drawing.Size(62, 25);
            this.lbPrix.TabIndex = 2;
            this.lbPrix.Text = "Prix :";
            // 
            // lbStock
            // 
            this.lbStock.AutoSize = true;
            this.lbStock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbStock.Location = new System.Drawing.Point(6, 124);
            this.lbStock.Name = "lbStock";
            this.lbStock.Size = new System.Drawing.Size(80, 25);
            this.lbStock.TabIndex = 1;
            this.lbStock.Text = "Stock :";
            // 
            // LbNom
            // 
            this.LbNom.AutoSize = true;
            this.LbNom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.LbNom.Location = new System.Drawing.Point(6, 80);
            this.LbNom.Name = "LbNom";
            this.LbNom.Size = new System.Drawing.Size(69, 25);
            this.LbNom.TabIndex = 0;
            this.LbNom.Text = "Nom :";
            // 
            // btnEnregistre
            // 
            this.btnEnregistre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(5)))), ((int)(((byte)(55)))));
            this.btnEnregistre.FlatAppearance.BorderSize = 0;
            this.btnEnregistre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistre.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnEnregistre.Location = new System.Drawing.Point(149, 408);
            this.btnEnregistre.Name = "btnEnregistre";
            this.btnEnregistre.Size = new System.Drawing.Size(199, 54);
            this.btnEnregistre.TabIndex = 33;
            this.btnEnregistre.Text = "Enregistre ";
            this.btnEnregistre.UseVisualStyleBackColor = false;
            this.btnEnregistre.Click += new System.EventHandler(this.btnEnregistre_Click);
            // 
            // FRM_Produit_Commande
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(500, 491);
            this.Controls.Add(this.btnEnregistre);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FRM_Produit_Commande";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Produit ";
            this.Load += new System.EventHandler(this.FRM_Produit_Commande_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textRemise;
        private System.Windows.Forms.TextBox textQuantité;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbPrix;
        private System.Windows.Forms.Label lbStock;
        private System.Windows.Forms.Label LbNom;
        private System.Windows.Forms.Button btnEnregistre;
        public System.Windows.Forms.Label Prix;
        public System.Windows.Forms.Label stock;
        public System.Windows.Forms.Label nom;
        public System.Windows.Forms.TextBox textTotale;
        public System.Windows.Forms.Label textId;
        private System.Windows.Forms.Label id;
    }
}