﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class FRM_Client_Commande : Form
    {
        dbStockContext bd;
        public FRM_Client_Commande()
        {
            InitializeComponent();
            bd = new dbStockContext();
        }
        public void Actualiserdatagrid()
        {
            bd = new dbStockContext();
            dvgClient.Rows.Clear(); // valider datagrid view 
            foreach (var S in bd.Client)
            {   // ajouter les lignes dans datagrid 
                dvgClient.Rows.Add( S.ID_Client, S.Nom_client, S.Prenom_Client, S.Adresse_Client, S.telephone_Client, S.Email_Client, S.Ville_CLient, S.Pays_Client);
            }
        }
        private void FRM_Client_Commande_Load(object sender, EventArgs e)
        {
            Actualiserdatagrid();
        }

        private void dvgClient_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Close();
        }
    }
}
