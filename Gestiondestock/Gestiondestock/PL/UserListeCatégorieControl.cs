﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Microsoft.Office.Interop.Excel;

namespace Gestiondestock.PL
{
    public partial class UserListeCatégorieControl : UserControl
    {
        private dbStockContext bd;
        private static UserListeCatégorieControl UserCategorie;
        // Creer un instance controle 
        public static UserListeCatégorieControl Instance
        {
            get
            {
                if (UserCategorie == null)
                {
                    UserCategorie = new UserListeCatégorieControl();
                }
                return UserCategorie;
            }
        }
        public UserListeCatégorieControl()
        {
            InitializeComponent();
            bd = new dbStockContext();
        }
        public void Remplirdatagrid()
        {
            bd = new dbStockContext();

            DvgCatégorie.Rows.Clear(); // valider datagrid view 

            foreach (var S in bd.Categorie)
            {
                DvgCatégorie.Rows.Add(false, S.ID_GATEGORIE, S.Nom_Categorie); // Cat.Nom_categorie Pour afficher le nom de categorie


            }
        }
        public string SelectVerif()
        {
            int Nombreligneselect = 0;
            for (int i = 0; i < DvgCatégorie.Rows.Count; i++)
            {
                object value = DvgCatégorie.Rows[i].Cells[0].Value;
                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    Nombreligneselect++; // nombreligneselet +1 

                }
            }
            if (Nombreligneselect == 0)
            {
                return "Selectionner Catégorie";

            }
            if (Nombreligneselect > 1)
            {
                return " Selectionner seulement 1 seul Catégorie  ";

            }
            return null;

        }

        private void textBoxRecherche_TextChanged(object sender, EventArgs e)
        {
            var maliste = bd.Categorie.ToList();
            if (textBoxRecherche.Text != "Recherche")
            {
                maliste = maliste.Where(s => s.Nom_Categorie.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                DvgCatégorie.Rows.Clear();
                foreach (var l in maliste)
                {
                    DvgCatégorie.Rows.Add(false, l.ID_GATEGORIE, l.Nom_Categorie);
                }
            }
                
        }

        private void textBoxRecherche_Enter(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "Recherche")
            {
                textBoxRecherche.Text = "";
                textBoxRecherche.ForeColor = Color.Black;
            }
        }

        private void textBoxRecherche_Leave(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "")
            {
                textBoxRecherche.Text = "Recherche";
                textBoxRecherche.ForeColor = Color.Silver;
            }
        }
        

        private void UserListeCatégorieControl_Load(object sender, EventArgs e)
        {
            Remplirdatagrid();
        }

        private void btnAjouterProduit_Click(object sender, EventArgs e)
        {
            PL.FRM_Ajouter_Modifier_Catégorie frmCat = new FRM_Ajouter_Modifier_Catégorie(this);
            frmCat.ShowDialog();
        }

        private void DvgCatégorie_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            PL.FRM_Ajouter_Modifier_Catégorie frmCat = new FRM_Ajouter_Modifier_Catégorie(this);
            if (DvgCatégorie.Columns[e.ColumnIndex].Name == "Modifier") // su j'au click sur button modifier 
            {
                //afficher Nom de catégorie formulaire pour modifier
                frmCat.LBtitreCatégorie.Text = "Modifier Catégorie";
                frmCat.textNomCatégorie.Text = DvgCatégorie.Rows[e.RowIndex].Cells[2].Value.ToString();
                frmCat.IdSelect = (int)DvgCatégorie.Rows[e.RowIndex].Cells[1].Value;
                frmCat.ShowDialog();

            }
            if (DvgCatégorie.Columns[e.ColumnIndex].Name == "Supprimer")
            {
                BL.CLS_Categorie ClCategorie = new BL.CLS_Categorie();
                int idcat;
                int p;
                DialogResult R = MessageBox.Show("Voulez-vous vraiment supprimer ce catégorie", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (R == DialogResult.Yes)
                {
                    idcat = (int)DvgCatégorie.Rows[e.RowIndex].Cells[1].Value;
                    p = bd.Produit.Count(s => s.ID_GATEGORIE == idcat);
                    if (p == 0)
                    {
                        ClCategorie.suprimer_Categorie(idcat);
                        Remplirdatagrid();
                        MessageBox.Show("Suppresion avec succées ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        DialogResult PDF = MessageBox.Show("Il ya "+p+"produit dans cette catégorie voulez vous vraiment supprimer", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if(PDF== DialogResult.Yes)
                        {
                            ClCategorie.suprimer_Categorie(idcat);
                            MessageBox.Show("Suppresion avec succées ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            Remplirdatagrid();
                           
                        }
                        else
                        {
                            MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                      

                }
                else
                {
                    MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               
            }
            else
            {
                MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnImprimerToutes_Click(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            var listeCategorie = bd.Categorie.ToList();
            try
            {
                RPT.RPAfficher frrptC = new RPT.RPAfficher();
                frrptC.reportViewer1.LocalReport.ReportEmbeddedResource = "Gestiondestock.RPT.RPT_Listes_Categorie.rdlc"; // chemin de rapporte 
                                                                                                                          // ajouter data source 
                frrptC.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSetCategorie", listeCategorie));
                // ajouter date 
                ReportParameter date = new ReportParameter("Date", DateTime.Now.ToShortDateString());
                ReportParameter NbrCatégorie = new ReportParameter("NbrCategorie", bd.Categorie.Count().ToString());// 
                frrptC.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { date , NbrCatégorie });
                frrptC.reportViewer1.RefreshReport();
                frrptC.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public String NomCategorie;
        public int idCategorie;
        private void btnSauvgarderdansUnexecl_Click(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            if (SelectVerif() != null)
            {
                MessageBox.Show(SelectVerif(), "Imprimer Produit", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {


                using (SaveFileDialog SFD = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx", ValidateNames = true })  // Filtrer seulement fichier excel
                {
                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                        Workbook wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
                        Worksheet ws = (Worksheet)app.ActiveSheet;
                        app.Visible = false;
                        //Nom de catégorie et id categorie
                        for (int j = 0; j < DvgCatégorie.Rows.Count; j++)
                        {
                            object value = DvgCatégorie.Rows[j].Cells[0].Value;
                            if (value != null && (Boolean)value) // si le ligne est selectionner 
                            {
                                NomCategorie = DvgCatégorie.Rows[j].Cells[2].Value.ToString();
                                idCategorie = (int)DvgCatégorie.Rows[j].Cells[1].Value;
                            }
                        }
                        // ecrire Nom de catégorie dans fichier excel
                        ws.Range["A1:D1"].Merge();
                        ws.Range["A1:D1"].Value = NomCategorie;
                        // ajouter les lignes de fichier execel
                        ws.Cells[2, 1] = "Id produit";
                        ws.Cells[2, 2] = "Nom produit";
                        ws.Cells[2, 3] = "Quantité";
                        ws.Cells[2, 4] = "Prix";
                        // liste produit dans cette categorie 
                        var listeProduit = bd.Produit.Where(s => s.ID_GATEGORIE == idCategorie).ToList();
                        int i = 3;
                        foreach (var L in listeProduit)
                        {
                            ws.Cells[i, 1] = L.Id_Produit;
                            ws.Cells[i, 2] = L.Nom_Produit;
                            ws.Cells[i, 3] = L.Quantite_Produit;
                            ws.Cells[i, 4] = L.Prix_Produit;
                            i++;


                        }


                        // changer style de fichier ----
                        ws.Range["A2:D2"].Interior.Color = Color.Teal;
                        ws.Range["A2:D2"].Font.Color = Color.White;
                        ws.Range["A2:D2"].Font.Size = 15;
                        // categorie 
                        ws.Range["A1:D1"].Interior.Color = Color.DarkGreen;
                        ws.Range["A1:D1"].Font.Color = Color.White;
                        ws.Range["A1:D1"].Font.Size = 15;
                        //centrer txt
                        ws.Range["A:D"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        ws.Range["A2:D2"].ColumnWidth = 16;

                        wb.SaveAs(SFD.FileName, XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false); // sauv dans fichier excel
                        app.Quit();
                        MessageBox.Show("Sauvgarder avec sauccées dans le Excel", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                }
            }  }
    }
}
