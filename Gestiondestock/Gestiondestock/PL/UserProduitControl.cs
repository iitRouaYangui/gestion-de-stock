﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Reporting.WinForms;
using Microsoft.Office.Interop.Excel;

namespace Gestiondestock.PL
{
    public partial class UserProduitControl : UserControl
    {
        private dbStockContext bd;
        private static UserProduitControl UserProduit;
        // Creer un instance controle 
        public static UserProduitControl Instance
        {
            get
            {
                if (UserProduit == null)
                {
                    UserProduit = new UserProduitControl();
                }
                return UserProduit;
            }
        }
        public UserProduitControl()
        {
            InitializeComponent();
            bd = new dbStockContext();
        }
        public void Actualiserdatagrid()
        {
            bd = new dbStockContext();

            dvgProduit.Rows.Clear(); // valider datagrid view 
            // pour afficher le nom de catégorie a partie de id categorie 
            Categorie cat = new Categorie();

            foreach (var S in bd.Produit)
            {
                cat = bd.Categorie.SingleOrDefault(s => s.ID_GATEGORIE == S.ID_GATEGORIE); // si le idcategorie dans produit = id categorie dans categorie 
                // ajouter les lignes dans datagrid 
                if (cat != null)
                {
                    dvgProduit.Rows.Add(false, S.Id_Produit, S.Nom_Produit, S.Quantite_Produit, S.Prix_Produit, cat.Nom_Categorie); // Cat.Nom_categorie Pour afficher le nom de categorie
                }

            }
            // si qte =0 en rouge else vert
            for (int i = 0; i < dvgProduit.Rows.Count; i++)
            {
                object value = dvgProduit.Rows[i].Cells[3].Value;
                if (value != null && (int)value==0)
                {
                    dvgProduit.Rows[i].Cells[3].Style.BackColor = Color.Red;                 

                }
                else
                {
                    dvgProduit.Rows[i].Cells[3].Style.BackColor = Color.LightGreen;
                }
            }

        }

        // verif combien de ligne selectionner
        public string SelectVerif()
        {
            int Nombreligneselect = 0;
            for (int i = 0; i < dvgProduit.Rows.Count; i++)
            {
                object value = dvgProduit.Rows[i].Cells[0].Value;
                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    Nombreligneselect++; // nombreligneselet +1 

                }
            }
            if (Nombreligneselect == 0)
            {
                return "Selectionner le Produit";

            }
            if (Nombreligneselect > 1)
            {
                return " Selectionner seulement 1 seul Produit  ";

            }
            return null;

        }
        private void dvgClient_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void UserProduitControl_Load(object sender, EventArgs e)
        {
            Actualiserdatagrid();
        }

        private void btnImprimerPRcocher_Click(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            int idSelect=0;
            string NomCategorie =null;
            RPT.RPAfficher frmrpt = new RPT.RPAfficher();
            Produit PR = new Produit();
            if(SelectVerif()!=null)
            {
                MessageBox.Show(SelectVerif(), "Imprimer Produit",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                for (int i = 0; i < dvgProduit.Rows.Count; i++)
                {
                    object value = dvgProduit.Rows[i].Cells[0].Value;
                    if (value != null && (Boolean)value) // si le ligne est selectionner 
                    {
                        idSelect = (int)dvgProduit.Rows[i].Cells[1].Value;
                        NomCategorie = (String)dvgProduit.Rows[i].Cells[5].Value.ToString();
                    }
                }
                    PR = bd.Produit.SingleOrDefault(s=>s.Id_Produit == idSelect);
                    if(PR!=null)
                    {   // donner le rapporte 
                        frmrpt.reportViewer1.LocalReport.ReportEmbeddedResource = "Gestiondestock.RPT.RPT_Produit.rdlc"; // chemin de rapporte 
                        // ajouter using microsoft.reporting.winform
                        ReportParameter Pcategorie = new ReportParameter("RP_Catégorie", NomCategorie); // Nom de catégorie
                        ReportParameter PNom = new ReportParameter("RP_Nom",PR.Nom_Produit); // Nom de Produit
                        ReportParameter Pquantite = new ReportParameter("RP_Quantite", PR.Quantite_Produit.ToString()); // qte
                        ReportParameter PPrix = new ReportParameter("RP_Prix", PR.Prix_Produit.ToString()); // prix
                        // conv image en string
                        string ImageString = Convert.ToBase64String(PR.Image_Produit);
                        ReportParameter PImage = new ReportParameter("RP_Img", ImageString);
                        // Sauv les nv paramettre dans la rapport 
                        frmrpt.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { Pcategorie, PNom, Pquantite, PPrix, PImage });
                        frmrpt.reportViewer1.RefreshReport();
                        frmrpt.ShowDialog();






                    
                }
            }
        }

        private void textBoxRecherche_Enter(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "Recherche")
            {
                textBoxRecherche.Text = "";
                textBoxRecherche.ForeColor = Color.Black;
            }
        }

        private void textBoxRecherche_TextChanged(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            var listerecherche = bd.Produit.ToList(); // liste recherche = liste des produit
            // recherche suelement par nom de produit
            if (textBoxRecherche.Text != "Recherche")
            {
                listerecherche = listerecherche.Where(s => s.Nom_Produit.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                //vider data grid 
                dvgProduit.Rows.Clear();
                Categorie cat = new Categorie();
                // ajouter listerecherche dans la datagrid clien
                foreach (var l in listerecherche)
                {
                    cat = bd.Categorie.SingleOrDefault(s => s.ID_GATEGORIE == l.ID_GATEGORIE);
                    dvgProduit.Rows.Add(false, l.Id_Produit, l.Nom_Produit, l.Quantite_Produit, l.Prix_Produit, cat.Nom_Categorie);
                }
            }
               

        }

        private void textBoxRecherche_Leave(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "")
            {
                textBoxRecherche.Text = "Recherche";
                textBoxRecherche.ForeColor = Color.Silver;
            }
        }

        private void btnAjouterProduit_Click(object sender, EventArgs e)
        {
            PL.FRM_Ajouter_Modifier_Produits frmProduit = new FRM_Ajouter_Modifier_Produits(this);
            frmProduit.ShowDialog();
        }

        private void btnModifierProduit_Click(object sender, EventArgs e)
        {
            Produit PR = new Produit();

            if (SelectVerif() == null)
            {
                PL.FRM_Ajouter_Modifier_Produits frmProduit = new PL.FRM_Ajouter_Modifier_Produits(this);
                frmProduit.LBtitreProduit.Text = "Modifier Produit ";
                frmProduit.btnActualiserProduit.Visible = false;

                for (int i = 0; i < dvgProduit.Rows.Count; i++)
                {

                    object value = dvgProduit.Rows[i].Cells[0].Value;
                    if (value != null && (Boolean)value) // si le ligne est selectionner 
                    {
                        int MYIDSELECT = (int)dvgProduit.Rows[i].Cells[1].Value; //MYIDSELECT=id de ligne selectionner
                        PR = bd.Produit.Single(s => s.Id_Produit == MYIDSELECT);  //pour vérifier si le id de produit existe
                        if (PR != null)
                        {
                            frmProduit.IdSelect = (int)dvgProduit.Rows[i].Cells[1].Value;
                            frmProduit.textPrenomProduit.Text = dvgProduit.Rows[i].Cells[2].Value.ToString();
                            frmProduit.textQTE.Text = dvgProduit.Rows[i].Cells[3].Value.ToString();
                            frmProduit.textPrix.Text = dvgProduit.Rows[i].Cells[4].Value.ToString();
                            // afficher image de produit modifier
                            MemoryStream MS = new MemoryStream(PR.Image_Produit); // pour convertir l'image
                            frmProduit.pictureBox1.Image = Image.FromStream(MS);
                        }

                    }
                }

                frmProduit.ShowDialog();
                frmProduit.Close();

            }
            else
            {
                MessageBox.Show(SelectVerif(), "Modifier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnafficherphotoProduit_Click(object sender, EventArgs e)
        {
            Produit PR = new Produit();
            if (SelectVerif() == null)
            {
                for (int i = 0; i < dvgProduit.Rows.Count; i++) //verifie ligne selectionner
                {
                    object value = dvgProduit.Rows[i].Cells[0].Value;
                    if (value != null && (Boolean)value)//si ligne selectionner
                    {
                        int MYIDSELECT = (int)dvgProduit.Rows[i].Cells[1].Value; //MYIDSELECT=id de ligne selectionner
                        PR = bd.Produit.Single(s => s.Id_Produit == MYIDSELECT);  //pour vérifier si le id de produit existe
                        if (PR != null)
                        {
                            FRM_Photo_Produit FrmP = new FRM_Photo_Produit();
                            // declaration systeme.io
                            MemoryStream MS = new MemoryStream(PR.Image_Produit);//pour conv image de produit
                            FrmP.Picture.Image = Image.FromStream(MS);
                            FrmP.LBtitreImage.Text = dvgProduit.Rows[i].Cells[2].Value.ToString();
                            //  afficher formulaire
                            FrmP.ShowDialog();
                        }
                    }
                }

            }
            else
            {
                MessageBox.Show(SelectVerif(), "Modifier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnsupprimerProduit_Click(object sender, EventArgs e)
        {
            BL.CLS_Produits ClClient = new BL.CLS_Produits();
            if (SelectVerif() == "Selectionner le Produit")
            {
                MessageBox.Show("aucun client selectionner", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                DialogResult R = MessageBox.Show("Voulez-vous vraiment supprimer ce client", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (R == DialogResult.Yes)
                {   //Pour supprimer toutes les client selectionner
                    for (int j = 0; j < dvgProduit.Rows.Count; j++)
                    {
                        object val = dvgProduit.Rows[j].Cells[0].Value;
                        if (val != null && (Boolean)val) // si le ligne est selectionner 
                        {
                            ClClient.suprimer_Client(int.Parse(dvgProduit.Rows[j].Cells[1].Value.ToString()));
                        }
                    }
                    // actualiser datagrod view
                    Actualiserdatagrid();
                    MessageBox.Show("Suppresion avec succées ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }


        }

        private void btnModifierClient_Click(object sender, EventArgs e)
        {
            bd = new dbStockContext();
            var listeproduit = bd.Produit.ToList();
            try
            {
                RPT.RPAfficher frrpt = new RPT.RPAfficher();
                frrpt.reportViewer1.LocalReport.ReportEmbeddedResource = "Gestiondestock.RPT.Liste_Produit.rdlc"; // chemin de rapporte 
                                                                                                                  // ajouter data source 
                frrpt.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", listeproduit));
                // ajouter date 
                ReportParameter date = new ReportParameter("Date", DateTime.Now.ToString()); // 
                frrpt.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { date });
                frrpt.reportViewer1.RefreshReport();
                frrpt.ShowDialog();
            }catch(Exception ex )
            {
                MessageBox.Show(ex.Message);
            }
           


        }

        private void btnSauvgarderdansUnexecl_Click(object sender, EventArgs e)
        {
            bd = new dbStockContext();
           
            using (SaveFileDialog SFD = new SaveFileDialog() { Filter = "Excel Workbook|*.xlsx" ,ValidateNames=true})  // Filtrer seulement fichier excel
            {
                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                    Workbook wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
                    Worksheet ws = (Worksheet)app.ActiveSheet;
                    app.Visible = false;
                    // ajouter les lignes de fichier execel
                    ws.Cells[1, 1] = "Id produit";
                    ws.Cells[1, 2] = "Nom produit";
                    ws.Cells[1, 3] = "Quantité";
                    ws.Cells[1, 4] = "Prix";
                    // ajouter liste de produit de BD dans fichier excel
                    var ListeProduit = bd.Produit.ToList();
                    int i = 2;
                    foreach( var L in ListeProduit)
                    {
                        ws.Cells[i, 1] = L.Id_Produit;
                        ws.Cells[i, 2] = L.Nom_Produit;
                        ws.Cells[i, 3] = L.Quantite_Produit;
                        ws.Cells[i, 4] = L.Prix_Produit;
                        i++;
                        

                    }
                    // changer style de fichier ----
                    ws.Range["A1:D1"].Interior.Color = Color.Teal;
                    ws.Range["A1:D1"].Font.Color = Color.White;
                    ws.Range["A1:D1"].Font.Size = 15;
                    //centrer txt
                    ws.Range["A1:D1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    ws.Range["A1:D1"].ColumnWidth = 16;

                    wb.SaveAs(SFD.FileName); // sauv dans fichier excel
                    app.Quit();
                    MessageBox.Show("Sauvgarder avec sauccées dans le Excel", "Excel", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    

                }
            }
        }

        private void btnActualiser_Click(object sender, EventArgs e)
        {
            Actualiserdatagrid();
        }
    }
}
