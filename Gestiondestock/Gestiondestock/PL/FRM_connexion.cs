﻿using System;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
    public partial class FRM_connexion : Form
    {
        private dbStockContext bd;
        // classe conx 
        BL.CLS_Connexion C = new BL.CLS_Connexion();
        private Form fremenu;
        public FRM_connexion( Form Menu)
        {
            InitializeComponent();
            this.fremenu = Menu;
            // initialiser le base de données 
            bd = new dbStockContext();
            
        }
        // pour verifier les camp obligatoires 
        string testobligatoire()
        {
            if (textNomUtilisateur.Text=="" || textNomUtilisateur.Text == "Nom d'utilisateur")
            {
                return "Entrer votre nom";
            }
            if (textPass.Text == "" || textPass.Text== "Mode de passe")
            {
                return "Entrer votre Mode de passe ";
            }
            return null;
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void textNomUtilisateur_Enter(object sender, EventArgs e)
        {
          if(textNomUtilisateur.Text== "Nom d'utilisateur")
            {
                textNomUtilisateur.Text = "";
                textNomUtilisateur.ForeColor = System.Drawing.Color.WhiteSmoke;
            }
        }

        private void textPass_Enter(object sender, EventArgs e)
        {
            if(textPass.Text=="Mode de passe")
            {
                textPass.Text = "";
                textPass.UseSystemPasswordChar = false;
                textPass.PasswordChar = '*';
                textPass.ForeColor = System.Drawing.Color.WhiteSmoke;
            }
        }

        private void textNomUtilisateur_Leave(object sender, EventArgs e)
        {
            if (textNomUtilisateur.Text == "")
            {
                textNomUtilisateur.Text = "Nom d'utilisateur";
                textNomUtilisateur.ForeColor = System.Drawing.Color.Silver;
            }
        }

        private void textPass_Leave(object sender, EventArgs e)
        {
            if (textPass.Text == "")
            {
                textPass.Text = "Mode de passe";
                textPass.UseSystemPasswordChar = true;
                textPass.ForeColor = System.Drawing.Color.WhiteSmoke;
            }
        }

        private void textNomUtilisateur_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (testobligatoire() == null)
            {
               if(C.ConnexionValide(bd,textNomUtilisateur.Text,textPass.Text) == true)
                {
                    MessageBox.Show(" conx réussi" ,"connexion",MessageBoxButtons.OK , MessageBoxIcon.Asterisk );
                    (fremenu as FRM_Menu).activerForm();
                    this.Close();
                }
               else
                {
                    MessageBox.Show("conx échoué ", "connexion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            else
                MessageBox.Show(testobligatoire(), "obligatoire", MessageBoxButtons.OK);
        }
    }
}
