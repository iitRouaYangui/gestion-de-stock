﻿
namespace Gestiondestock.PL
{
	partial class FRM_Menu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlBut = new System.Windows.Forms.Panel();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnUtilisateur = new System.Windows.Forms.Button();
            this.btnCommande = new System.Windows.Forms.Button();
            this.btnCatégorie = new System.Windows.Forms.Button();
            this.btnPtoduit = new System.Windows.Forms.Button();
            this.btnClient = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlSetting = new System.Windows.Forms.Panel();
            this.btnConnecter = new System.Windows.Forms.Button();
            this.btnDeconnecter = new System.Windows.Forms.Button();
            this.btnRestaurer = new System.Windows.Forms.Button();
            this.btncopy = new System.Windows.Forms.Button();
            this.pnlafiche = new System.Windows.Forms.Panel();
            this.btnSetting = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.pnlBut);
            this.panel1.Controls.Add(this.btnMenu);
            this.panel1.Controls.Add(this.btnUtilisateur);
            this.panel1.Controls.Add(this.btnCommande);
            this.panel1.Controls.Add(this.btnCatégorie);
            this.panel1.Controls.Add(this.btnPtoduit);
            this.panel1.Controls.Add(this.btnClient);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(282, 814);
            this.panel1.TabIndex = 0;
            // 
            // pnlBut
            // 
            this.pnlBut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.pnlBut.ForeColor = System.Drawing.Color.Transparent;
            this.pnlBut.Location = new System.Drawing.Point(1, 137);
            this.pnlBut.Name = "pnlBut";
            this.pnlBut.Size = new System.Drawing.Size(10, 66);
            this.pnlBut.TabIndex = 5;
            // 
            // btnMenu
            // 
            this.btnMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Image = global::Gestiondestock.Properties.Resources.Menu_;
            this.btnMenu.Location = new System.Drawing.Point(214, 3);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(66, 50);
            this.btnMenu.TabIndex = 5;
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnUtilisateur
            // 
            this.btnUtilisateur.FlatAppearance.BorderSize = 0;
            this.btnUtilisateur.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnUtilisateur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUtilisateur.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUtilisateur.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUtilisateur.Image = global::Gestiondestock.Properties.Resources.toppng_com_customer;
            this.btnUtilisateur.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUtilisateur.Location = new System.Drawing.Point(20, 604);
            this.btnUtilisateur.Name = "btnUtilisateur";
            this.btnUtilisateur.Size = new System.Drawing.Size(262, 66);
            this.btnUtilisateur.TabIndex = 9;
            this.btnUtilisateur.Text = "         Utilisateur";
            this.btnUtilisateur.UseVisualStyleBackColor = true;
            this.btnUtilisateur.Click += new System.EventHandler(this.btnUtilisateur_Click);
            // 
            // btnCommande
            // 
            this.btnCommande.FlatAppearance.BorderSize = 0;
            this.btnCommande.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnCommande.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommande.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCommande.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCommande.Image = global::Gestiondestock.Properties.Resources.PngItem;
            this.btnCommande.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCommande.Location = new System.Drawing.Point(20, 477);
            this.btnCommande.Name = "btnCommande";
            this.btnCommande.Size = new System.Drawing.Size(262, 74);
            this.btnCommande.TabIndex = 8;
            this.btnCommande.Text = "           Commande";
            this.btnCommande.UseVisualStyleBackColor = true;
            this.btnCommande.Click += new System.EventHandler(this.btnCommande_Click);
            // 
            // btnCatégorie
            // 
            this.btnCatégorie.FlatAppearance.BorderSize = 0;
            this.btnCatégorie.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnCatégorie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCatégorie.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCatégorie.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCatégorie.Image = global::Gestiondestock.Properties.Resources.categories1;
            this.btnCatégorie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCatégorie.Location = new System.Drawing.Point(20, 366);
            this.btnCatégorie.Name = "btnCatégorie";
            this.btnCatégorie.Size = new System.Drawing.Size(262, 59);
            this.btnCatégorie.TabIndex = 7;
            this.btnCatégorie.Text = "         Catégorie";
            this.btnCatégorie.UseVisualStyleBackColor = true;
            this.btnCatégorie.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnPtoduit
            // 
            this.btnPtoduit.FlatAppearance.BorderSize = 0;
            this.btnPtoduit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnPtoduit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPtoduit.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPtoduit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPtoduit.Image = global::Gestiondestock.Properties.Resources.shop_carticon;
            this.btnPtoduit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPtoduit.Location = new System.Drawing.Point(20, 242);
            this.btnPtoduit.Name = "btnPtoduit";
            this.btnPtoduit.Size = new System.Drawing.Size(262, 69);
            this.btnPtoduit.TabIndex = 6;
            this.btnPtoduit.Text = "       Produit";
            this.btnPtoduit.UseVisualStyleBackColor = true;
            this.btnPtoduit.Click += new System.EventHandler(this.btnPtoduit_Click);
            // 
            // btnClient
            // 
            this.btnClient.FlatAppearance.BorderSize = 0;
            this.btnClient.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClient.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClient.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnClient.Image = global::Gestiondestock.Properties.Resources.officeclient;
            this.btnClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClient.Location = new System.Drawing.Point(20, 137);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(262, 66);
            this.btnClient.TabIndex = 5;
            this.btnClient.Text = "       Client";
            this.btnClient.UseVisualStyleBackColor = true;
            this.btnClient.Click += new System.EventHandler(this.btnClient_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(282, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1077, 17);
            this.panel4.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlSetting);
            this.panel2.Controls.Add(this.pnlafiche);
            this.panel2.Controls.Add(this.btnSetting);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(282, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1077, 797);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // pnlSetting
            // 
            this.pnlSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(28)))), ((int)(((byte)(38)))));
            this.pnlSetting.Controls.Add(this.btnConnecter);
            this.pnlSetting.Controls.Add(this.btnDeconnecter);
            this.pnlSetting.Controls.Add(this.btnRestaurer);
            this.pnlSetting.Controls.Add(this.btncopy);
            this.pnlSetting.Location = new System.Drawing.Point(50, 0);
            this.pnlSetting.Name = "pnlSetting";
            this.pnlSetting.Size = new System.Drawing.Size(314, 44);
            this.pnlSetting.TabIndex = 10;
            this.pnlSetting.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSetting_Paint);
            // 
            // btnConnecter
            // 
            this.btnConnecter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnConnecter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnConnecter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnecter.Font = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnecter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnConnecter.Image = global::Gestiondestock.Properties.Resources.Connected_16;
            this.btnConnecter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnecter.Location = new System.Drawing.Point(-3, 0);
            this.btnConnecter.Name = "btnConnecter";
            this.btnConnecter.Size = new System.Drawing.Size(317, 36);
            this.btnConnecter.TabIndex = 0;
            this.btnConnecter.Text = "Connecter";
            this.btnConnecter.UseVisualStyleBackColor = true;
            this.btnConnecter.Click += new System.EventHandler(this.btnConnecter_Click);
            // 
            // btnDeconnecter
            // 
            this.btnDeconnecter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnDeconnecter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnDeconnecter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeconnecter.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeconnecter.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeconnecter.Image = global::Gestiondestock.Properties.Resources.Deconnecte;
            this.btnDeconnecter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeconnecter.Location = new System.Drawing.Point(-3, 134);
            this.btnDeconnecter.Name = "btnDeconnecter";
            this.btnDeconnecter.Size = new System.Drawing.Size(317, 48);
            this.btnDeconnecter.TabIndex = 3;
            this.btnDeconnecter.Text = "Déconnecter";
            this.btnDeconnecter.UseVisualStyleBackColor = true;
            this.btnDeconnecter.Click += new System.EventHandler(this.btnDeconnecter_Click);
            // 
            // btnRestaurer
            // 
            this.btnRestaurer.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnRestaurer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btnRestaurer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestaurer.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestaurer.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRestaurer.Image = global::Gestiondestock.Properties.Resources.download;
            this.btnRestaurer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRestaurer.Location = new System.Drawing.Point(0, 86);
            this.btnRestaurer.Name = "btnRestaurer";
            this.btnRestaurer.Size = new System.Drawing.Size(314, 48);
            this.btnRestaurer.TabIndex = 2;
            this.btnRestaurer.Text = "Restaurer une copie enregistrée ";
            this.btnRestaurer.UseVisualStyleBackColor = true;
            // 
            // btncopy
            // 
            this.btncopy.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btncopy.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(8)))), ((int)(((byte)(55)))));
            this.btncopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncopy.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncopy.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btncopy.Image = global::Gestiondestock.Properties.Resources.Copier;
            this.btncopy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btncopy.Location = new System.Drawing.Point(0, 37);
            this.btncopy.Name = "btncopy";
            this.btncopy.Size = new System.Drawing.Size(314, 45);
            this.btncopy.TabIndex = 1;
            this.btncopy.Text = "créer une copie de l\'application";
            this.btncopy.UseVisualStyleBackColor = true;
            this.btncopy.Click += new System.EventHandler(this.btncopy_Click);
            // 
            // pnlafiche
            // 
            this.pnlafiche.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlafiche.BackColor = System.Drawing.SystemColors.Control;
            this.pnlafiche.Location = new System.Drawing.Point(0, 50);
            this.pnlafiche.Name = "pnlafiche";
            this.pnlafiche.Size = new System.Drawing.Size(1077, 757);
            this.pnlafiche.TabIndex = 11;
            this.pnlafiche.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlafiche_Paint);
            // 
            // btnSetting
            // 
            this.btnSetting.FlatAppearance.BorderSize = 0;
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.Image = global::Gestiondestock.Properties.Resources.Settings_32;
            this.btnSetting.Location = new System.Drawing.Point(0, 0);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(44, 52);
            this.btnSetting.TabIndex = 9;
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::Gestiondestock.Properties.Resources.Subtract_32;
            this.button1.Location = new System.Drawing.Point(969, -3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 50);
            this.button1.TabIndex = 8;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::Gestiondestock.Properties.Resources.Shutdown_32;
            this.button2.Location = new System.Drawing.Point(1025, -3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 50);
            this.button2.TabIndex = 7;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // FRM_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1359, 814);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRM_Menu";
            this.Text = "FRM_Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FRM_Menu_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlSetting.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button btnClient;
		private System.Windows.Forms.Button btnUtilisateur;
		private System.Windows.Forms.Button btnCommande;
		private System.Windows.Forms.Button btnCatégorie;
		private System.Windows.Forms.Button btnPtoduit;
		private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Panel pnlBut;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlSetting;
        private System.Windows.Forms.Button btnDeconnecter;
        private System.Windows.Forms.Button btnRestaurer;
        private System.Windows.Forms.Button btncopy;
        private System.Windows.Forms.Button btnConnecter;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel pnlafiche;
    }
}