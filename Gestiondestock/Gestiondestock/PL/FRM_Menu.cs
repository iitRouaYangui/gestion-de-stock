﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestiondestock.PL
{
	public partial class FRM_Menu : Form
	{
		public FRM_Menu()
		{
			InitializeComponent();
			panel1.Size = new Size(282, 66);
			pnlSetting.Visible = false;
		}
		// desc le formulaire 
		public void desactiverForm()
        {
			btnCatégorie.Enabled = false;
			btnClient.Enabled = false;
			btnCommande.Enabled = false;
			btnPtoduit.Enabled = false;
			btnUtilisateur.Enabled = false;
			btnRestaurer.Enabled = false;
			btncopy.Enabled = false;
			btnDeconnecter.Enabled = false;
			btnConnecter.Enabled = true;
			pnlafiche.Visible = false;
		}
		// ativer formulaire 
		public void activerForm()
		{
			btnCatégorie.Enabled= true;
			btnClient.Enabled = true;
			btnCommande.Enabled = true;
			btnPtoduit.Enabled = true;
			btnUtilisateur.Enabled = true;
			btnRestaurer.Enabled = true;
			btncopy.Enabled = true;
			btnDeconnecter.Enabled = true;
			btnConnecter.Enabled = false;
			pnlafiche.Visible = true;

		}

		private void FRM_Menu_Load(object sender, EventArgs e)
		{
			desactiverForm();
		}
		private void button4_Click(object sender, EventArgs e)
		{
			pnlBut.Top = btnCatégorie.Top;
			if (!pnlafiche.Controls.Contains(UserListeCatégorieControl.Instance))
			{
				pnlafiche.Controls.Add(UserListeCatégorieControl.Instance);
				UserListeCatégorieControl.Instance.Dock = DockStyle.Fill;
				UserListeCatégorieControl.Instance.BringToFront();

			}
			else
			{
				UserListeCatégorieControl.Instance.BringToFront();


			}
		}

		private void btnUtilisateur_Click(object sender, EventArgs e)
		{
			pnlBut.Top = btnUtilisateur.Top;
		}

        private void button3_Click(object sender, EventArgs e)
        {
			if(panel1.Width== 282)
			{
				panel1.Size = new Size(84,814);
			}
			else
            {
				
				panel1.Size = new Size(282, 66);

			}
        }
		// nous avons utiliser un instance en partie user liste 
        private void btnClient_Click(object sender, EventArgs e) // cette methode pour afficher le frm ajoute_modifier 
        {
			pnlBut.Top = btnClient.Top;
			if(!pnlafiche.Controls.Contains(User_Liste_Client.Instance))
            {
				pnlafiche.Controls.Add(User_Liste_Client.Instance);
				User_Liste_Client.Instance.Dock = DockStyle.Fill;
				User_Liste_Client.Instance.BringToFront();

			}
			else
            {
				User_Liste_Client.Instance.BringToFront();
				

			}
        }

        private void btnPtoduit_Click(object sender, EventArgs e)
        {
			pnlBut.Top = btnPtoduit.Top;
			if (!pnlafiche.Controls.Contains(UserProduitControl.Instance))
			{
				pnlafiche.Controls.Add(UserProduitControl.Instance);
				UserProduitControl.Instance.Dock = DockStyle.Fill;
				UserProduitControl.Instance.BringToFront();

			}
			else
			{
				UserProduitControl.Instance.BringToFront();


			}
		}

        private void btnCommande_Click(object sender, EventArgs e)
        {
			pnlBut.Top = btnCommande.Top;
			if (!pnlafiche.Controls.Contains(UserListeCommandeControl.Instance))
			{
				pnlafiche.Controls.Add(UserListeCommandeControl.Instance);
				UserListeCommandeControl.Instance.Dock = DockStyle.Fill;
				UserListeCommandeControl.Instance.BringToFront();

			}
			else
			{
				UserListeCommandeControl.Instance.BringToFront();


			}
		}

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
			Application.Exit();
		}

        private void button1_Click_1(object sender, EventArgs e)
        {
			this.WindowState = FormWindowState.Minimized;
		}

        private void pnlSetting_Paint(object sender, PaintEventArgs e)
        {
			
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
			pnlSetting.Size = new Size(236,149);
			pnlSetting.Visible = !pnlSetting.Visible;
        }

        private void btncopy_Click(object sender, EventArgs e)
        {

        }

        private void btnConnecter_Click(object sender, EventArgs e)
        {
			// affiche le formaulaire de conx
			FRM_connexion frmC = new FRM_connexion(this);
			frmC.ShowDialog();
        }

        private void btnDeconnecter_Click(object sender, EventArgs e)
        {
			desactiverForm();
			pnlafiche.Visible = false;


		}

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlafiche_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
