﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Gestiondestock.PL
{
    public partial class FRM_Ajouter_Modifier_Produits : Form
    {
        private dbStockContext bd;
        private  UserControl UserProduit;
        public FRM_Ajouter_Modifier_Produits(UserControl user)
        {
            InitializeComponent();
            bd = new dbStockContext();
            this.UserProduit = user;
            // afficher les categorie comoboxctegorie
            comboxrecherche.DataSource = bd.Categorie.ToList();
            //pour filtrer seulement les noms de categories 
            comboxrecherche.DisplayMember = "Nom_Categorie"; // afficher les nom de catégorie 
            comboxrecherche.ValueMember = "ID_GATEGORIE";
        }
        string TestObligatoire()
        {
            if (textPrenomProduit.Text == "" || textPrenomProduit.Text == "Nom Produit")
            {
                return "Entrer Le Nom de Produit";
            }
            if (textQTE.Text == "" || textQTE.Text == "Quantite")
            {
                return "Entrer  Quantite ";
            }
            if (textPrix.Text == "" || textPrix.Text == "Prix")
            {
                return "Entrer  Prix ";
            }
            if (comboxrecherche.Text == "")
            {
                return "Entrer Catégorie";
            }
            if (pictureBox1.Image == null)
            {
                return "Entrer L'image de produit ";
            }



            return null;
        }

        private void comboxrecherche_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textPrenomProduit_Enter(object sender, EventArgs e)
        {
            if (textPrenomProduit.Text == "Nom Produit")
            {
                textPrenomProduit.Text = "";
                textPrenomProduit.ForeColor = Color.White;
            }
        }

        private void textPrenomProduit_Leave(object sender, EventArgs e)
        {
            if (textPrenomProduit.Text == "")
            {
                textPrenomProduit.Text = "Nom Produit";
                textPrenomProduit.ForeColor = Color.Silver;
            }
        }

        private void textQTE_Enter(object sender, EventArgs e)
        {
            if (textQTE.Text == "Quantite")
            {
                textQTE.Text = "";
                textQTE.ForeColor = Color.White;
            }
        }

        private void textQTE_Leave(object sender, EventArgs e)
        {
            if (textQTE.Text == "")
            {
                textQTE.Text = "Quantite";
                textQTE.ForeColor = Color.Silver;
            }

        }

        private void textPrix_Enter(object sender, EventArgs e)
        {
            if (textPrix.Text == "Prix")
            {
                textPrix.Text = "";
                textPrix.ForeColor = Color.White;
            }
        }

        private void textPrix_Leave(object sender, EventArgs e)
        {
            if (textPrix.Text == "")
            {
                textPrix.Text = "Prix";
                textPrix.ForeColor = Color.Silver;
            }
        }

        private void btnannuller_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnParcourire_Click(object sender, EventArgs e)
        {
            // afficher fichier Image 
            OpenFileDialog OP = new OpenFileDialog();
            OP.Filter = "|*.JPG;*.PNG;*.BMP"; // pour afficher seulement les images 
            if (OP.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = System.Drawing.Image.FromFile(OP.FileName);

            }
        }

        private void btnActualiserProduit_Click(object sender, EventArgs e)
        {
            textPrenomProduit.Text = "Nom Produit";
            textQTE.Text = "Quantite";
            textPrix.Text = "Prix";
            comboxrecherche.Text = "";
            pictureBox1.Image = null;
        }

        private void textQTE_KeyPress(object sender, KeyPressEventArgs e)
        {
            //textbox numeric
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }

        private void textPrix_KeyPress(object sender, KeyPressEventArgs e)
        {
            //textbox numeric
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }
        public int IdSelect;
        private void btnEnregistreProduit_Click(object sender, EventArgs e)
        {
            if (TestObligatoire() != null)
            {
                MessageBox.Show(TestObligatoire(), "Obligatoire", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (LBtitreProduit.Text == "Ajouter Produit")
                {
                    BL.CLS_Produits ClProduit = new BL.CLS_Produits(); // créer un objet pour stock dans BD
                    // convertire image en format byte 
                    // ajouter systeme.Io
                    MemoryStream MR = new MemoryStream();
                    pictureBox1.Image.Save(MR, pictureBox1.Image.RawFormat);
                    byte[] imageproduitbyte = MR.ToArray(); // convertir l'image en format byte 


                    if (ClProduit.Ajouter_Produit(textPrenomProduit.Text, int.Parse(textQTE.Text), textPrix.Text, imageproduitbyte, Convert.ToInt32(comboxrecherche.SelectedValue)))
                    {
                        MessageBox.Show("Produit Ajouter avec succes", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        (UserProduit as UserProduitControl).Actualiserdatagrid();

                    }
                    else
                    {
                        MessageBox.Show("produit déjà existant", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MemoryStream MR = new MemoryStream();
                    pictureBox1.Image.Save(MR, pictureBox1.Image.RawFormat);
                    byte[] imageproduitbyte = MR.ToArray();
                    BL.CLS_Produits ClProduit = new BL.CLS_Produits();
                    DialogResult R = MessageBox.Show("Voulez-vous vraiment modifier ce Produit", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (R == DialogResult.Yes)
                    {
                        ClProduit.modifier_Produit(IdSelect, textPrenomProduit.Text, int.Parse(textQTE.Text), textPrix.Text, imageproduitbyte, Convert.ToInt32(comboxrecherche.SelectedValue));
                        MessageBox.Show("Produit Modifier avec succés", "Modification", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        // pour actualiser  datagrid view
                        (UserProduit as UserProduitControl).Actualiserdatagrid();
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Modification est anullé ", "Modification ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textPrenomProduit_TextChanged(object sender, EventArgs e)
        {

        }
    }
}